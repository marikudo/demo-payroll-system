<?
class myDate{
	public function __construct(){

	}

	public function diff($dateStart,$dateEnd){
		$result = array();
			if ($dateStart!="" && $dateEnd!="") {
				$start_date = new DateTime($dateStart);
				$since_start = $start_date->diff(new DateTime($dateEnd));
				$result['days'] = $since_start->d;
				$result['hrs'] = $since_start->h;
				$result['mins'] = $since_start->i;
			}

			return $result;
	}
}