<?php

class payslip extends crackerjack{
	
	public function __construct(){
		parent::__construct();
		if ($this->session->_get('xadminlogin')==false) { redirect('xadmin/home/auth');}
		
	}
	public function index(){

		if($this->session->_get('message')==1){
			if($this->session->_get('action')=='update'){
				$data['success'] = '<div class="alert alert-success" style="margin-top: 5px;margin-bottom: 5px;" data-fade="3000">Position was successfully updated.<button type="button" class="close fade" data-dismiss="alert">&times;</button></div>';
			}
			if($this->session->_get('action')=='add'){
				$data['success'] = '<div class="alert alert-success" style="margin-top: 5px;margin-bottom: 5px;" data-fade="3000">Position was successfully added.<button type="button" class="close fade" data-dismiss="alert">&times;</button></div>';
			}
		$this->session->_set(array('message'=>false,'action'=>''));
		}

		$data['payslip'] = $this->crud->read("SELECT * FROM _tpayslips ",array(),'obj');
		$this->template->_admin('xadmin/payslip',$data,$this->load);
		//$this->template->adminTemplate('xadmin/dtr',$data,$this->load);
	}

	public function view($params = false){
			$id = $this->hash->decryptMe_($params[0]);

			if($id){
				$this->load->libraries(array('compensation'));
					$result = $this->crud->read("SELECT * FROM _tpayslips WHERE payslip_id=:payslip_id",array(":payslip_id"=>$id),'assoc');
					
					$StartDate = $result['date_start'];
					$EndDate = $result['date_end'];
					$dtrsummary = $this->crud->read("SELECT *, dtr.employee_id, dtr.eid,sum(hrs) as totalHrs, sum(mins) as totalMins, sum(late_hrs) as totallateHrs, sum(late_mins) as totallateMins, sum(total_in_mins) as totalrhrs, sum(total_late_in_mins) as totallates FROM _tdailytimerecord as dtr INNER JOIN _temployee AS te ON dtr.employee_id = te.employee_id WHERE _date BETWEEN :StartDate AND :EndDate AND payroll_status = 1 GROUP BY dtr.employee_id",array(":StartDate"=>$StartDate,":EndDate"=>$EndDate),'obj');
					//print_r($dtrsummary);
					$lates = 0;
					$payroll = array();
					$totalDeduction = 0;
					$sss = 0;
					$pagibig = 0;
					$philHealth = 0;
					$totalDeduction = 0;
					$netPay = 0;
					$basicPay = 0;
					$isUPdate = 0;
					foreach ($dtrsummary as $get) {

						/*get monthly rate*/
						$monthlyRate = $get->rate;
						/*devided by 2 cutoff*/
						$semimonthlyRate = ($get->rate / 2);
						/*get daily rate*/
						$dailyRate  = ($monthlyRate * 12) / 312;
						/*hourly rate*/
						$hourlyRate = $dailyRate / 8;
						/*minute rate*/
						$minuteRate = number_format( $hourlyRate / 60 , 2);
						//echo $get->totallates."=".$minuteRate."=".$hourlyRate."=".$dailyRate."<br />";
						$basicPay = $semimonthlyRate;
						/*overtime here*/

						/*deduction*/
						$lates = ($get->totallates * $minuteRate);
						$sss = $this->compensation->getSSS($semimonthlyRate) / 2;
						$pagibig = $this->compensation->getPagIbig($semimonthlyRate) / 2;
						//$philHealth = $this->compensation->getPhilHealth(double($monthlyRate)) / 2;;
						$philHealth = $this->compensation->getPhilHealth($semimonthlyRate) /2;
						$def = ($late + $sss + $pagibig + $philHealth);
						$taxable = $basicPay - ($def);
						$tax = $this->compensation->getTax($taxable,'single');
						$totalDeduction = ($def + $tax);

						//$tax."-".$totalDeduction."<br />";

						$netPay = ($basicPay - $totalDeduction);

						$payroll[$get->employee_id]['eid'] = $get->eid;
						$payroll[$get->employee_id]['fullname'] = $get->firstname." ".$get->lastname;
						$payroll[$get->employee_id]['NetPay'] = $netPay;
						$payroll[$get->employee_id]['basicPay'] = $basicPay;
						$payroll[$get->employee_id]['sss'] = $sss;
						$payroll[$get->employee_id]['pagibig'] = $pagibig;
						$payroll[$get->employee_id]['philHealth'] = $philHealth;
						$payroll[$get->employee_id]['tax'] = $tax;
						$payroll[$get->employee_id]['lates'] = $lates;
						$payroll[$get->employee_id]['totalDeduction'] = $totalDeduction;

						$isUPdate++;
						//$dtrsummary = $this->crud->read("UPDATE _t",array(":StartDate"=>$StartDate,":EndDate"=>$EndDate),'obj');
				


					}
					//print_r($payroll);
					$data['payroll'] = $payroll;
					$this->template->_admin('xadmin/payslip',$data,$this->load);
			}

	}

	public function printable($params = false){
			$id = $this->hash->decryptMe_($params[0]);

			if($id){
				$this->load->libraries(array('compensation'));
					$result = $this->crud->read("SELECT * FROM _tpayslips WHERE payslip_id=:payslip_id",array(":payslip_id"=>$id),'assoc');
					
					$StartDate = $result['date_start'];
					$EndDate = $result['date_end'];
					$dtrsummary = $this->crud->read("SELECT *,te.position_id, dtr.employee_id, dtr.eid,sum(hrs) as totalHrs, sum(mins) as totalMins, sum(late_hrs) as totallateHrs, sum(late_mins) as totallateMins, sum(total_in_mins) as totalrhrs, sum(total_late_in_mins) as totallates,sum(overtime) as overtime, sum(over_all_hrs) as over_all_hrs FROM _tdailytimerecord as dtr INNER JOIN _temployee AS te ON dtr.employee_id = te.employee_id WHERE _date BETWEEN :StartDate AND :EndDate AND payroll_status = 1 GROUP BY dtr.employee_id",array(":StartDate"=>$StartDate,":EndDate"=>$EndDate),'obj');
					//print_r($dtrsummary);
					$lates = 0;
					$payroll = array();
					$totalDeduction = 0;
					$sss = 0;
					$pagibig = 0;
					$philHealth = 0;
					$totalDeduction = 0;
					$netPay = 0;
					$basicPay = 0;
					$isUPdate = 0;
					foreach ($dtrsummary as $get) {

						/*get monthly rate*/
						$monthlyRate = $get->rate;
						/*devided by 2 cutoff*/
						$semimonthlyRate = ($get->rate / 2);
						/*get daily rate*/
						$dailyRate  = ($monthlyRate * 12) / 312;
						/*hourly rate*/
						$hourlyRate = $dailyRate / 8;
						/*minute rate*/
						$minuteRate = number_format( $hourlyRate / 60 , 2);
						$minuteRate2 =$hourlyRate / 60;
						//echo $get->totallates."=".$minuteRate."=".$hourlyRate."=".$dailyRate."<br />";
						$basicPay = $semimonthlyRate;
						/*overtime here*/

						/*deduction*/
						$lates = ($get->totallates * $minuteRate);
						$sss = $this->compensation->getSSS($semimonthlyRate) / 2;
						$pagibig = $this->compensation->getPagIbig($semimonthlyRate) / 2;
						//$philHealth = $this->compensation->getPhilHealth(double($monthlyRate)) / 2;;
						$overtime = $get->overtime * $minuteRate2;
						$philHealth = $this->compensation->getPhilHealth($semimonthlyRate) /2;
						$def = ($lates + $sss + $pagibig + $philHealth + $overtime);
						$taxable = $basicPay - ($def);
						$gross = $basicPay - ($lates) + $overtime;
						$tax = $this->compensation->getTax($taxable,'single');
						$totalDeduction = ($def + $tax);

						//$tax."-".$totalDeduction."<br />";

						$netPay = ($basicPay - $totalDeduction);
						$dept = $this->crud->read("SELECT * FROM _tposition WHERE position_id=:position_id",array(":position_id"=>$get->position_id),'assoc');
						$payroll[$get->employee_id]['department'] = $dept['position'];
						
						
						$payroll[$get->employee_id]['cutoff'] = $StartDate." - ".$EndDate;
						$payroll[$get->employee_id]['eid'] = $get->eid;
						$payroll[$get->employee_id]['fullname'] = $get->firstname." ".$get->lastname;
						$payroll[$get->employee_id]['NetPay'] = $netPay;
						$payroll[$get->employee_id]['basicPay'] = $basicPay;
						$payroll[$get->employee_id]['sss'] = $sss;
						$payroll[$get->employee_id]['taxable'] = $gross;
						$payroll[$get->employee_id]['overtime'] = $overtime;
						$payroll[$get->employee_id]['pagibig'] = $pagibig;
						$payroll[$get->employee_id]['philHealth'] = $philHealth;
						$payroll[$get->employee_id]['tax'] = $tax;
						$payroll[$get->employee_id]['lates'] = $lates;
						$payroll[$get->employee_id]['totalDeduction'] = $totalDeduction;

						$isUPdate++;
						//$dtrsummary = $this->crud->read("UPDATE _t",array(":StartDate"=>$StartDate,":EndDate"=>$EndDate),'obj');
				


					}
					//print_r($payroll);
					$data['payroll'] = $payroll;
					$this->load->render('xadmin/common/phead',$data);
					$this->load->render('xadmin/print',$data);
					$this->load->render('xadmin/common/pfooter',$data);
			}

	}
	
	public function submit(){
		if ($_POST) {
			$this->load->libraries(array('compensation'));

			$data['cutoff'] = $_POST['cutoff'];
			$cutoff = explode(":", $_POST['cutoff']);
			$d = explode("-", $cutoff[0]);
			 $year = ($d[0]==12 && $d[1] <= 31) ? date("Y") - 1 : date("y");

			 	$StartDate =  date("Y-m-d",strtotime($year."-".$cutoff[0]));
				$EndDate =  date("Y-m-d",strtotime("+".($cutoff[1]-1)." days",strtotime($StartDate)));

				$dtrsummary = $this->crud->read("SELECT *, dtr.employee_id, dtr.eid,sum(hrs) as totalHrs, sum(mins) as totalMins, sum(late_hrs) as totallateHrs, sum(late_mins) as totallateMins, sum(total_in_mins) as totalrhrs, sum(total_late_in_mins) as totallates FROM _tdailytimerecord as dtr INNER JOIN _temployee AS te ON dtr.employee_id = te.employee_id WHERE _date BETWEEN :StartDate AND :EndDate GROUP BY dtr.employee_id",array(":StartDate"=>$StartDate,":EndDate"=>$EndDate),'obj');
				$dtrsummaryx = $this->crud->read("SELECT * FROM _tdailytimerecord WHERE _date BETWEEN :StartDate AND :EndDate",array(":StartDate"=>$StartDate,":EndDate"=>$EndDate),'obj');
					$lates = 0;
					$payroll = array();
					$totalDeduction = 0;
					$sss = 0;
					$pagibig = 0;
					$philHealth = 0;
					$totalDeduction = 0;
					$netPay = 0;
					$basicPay = 0;
					$isUPdate = 0;
					foreach ($dtrsummary as $get) {

						$monthlyRate = $get->rate;
						$semimonthlyRate = ($get->rate / 2);

						$dailyRate  = ($monthlyRate * 12) / 365;
						$hourlyRate = $dailyRate / 8;
						$minuteRate = number_format( $hourlyRate / 60 , 2);
						//echo $get->totallates."=".$minuteRate."=".$hourlyRate."=".$dailyRate."<br />";
						$basicPay = $semimonthlyRate;
						/*overtime here*/

						/*deduction*/
						$lates = ($get->totallates * $minuteRate);
						$sss = $this->compensation->getSSS($monthlyRate) / 2;
						$pagibig = $this->compensation->getPagIbig($monthlyRate) / 2;
						//$philHealth = $this->compensation->getPhilHealth(double($monthlyRate)) / 2;;
						$philHealth = $this->compensation->getPhilHealth($monthlyRate) / 2;;

						$totalDeductionN = ($late + $sss + $pagibig + $philHealth);
						$tax = $this->compensation->getTax(($basicPay - $totalDeductionN));
						$totalDeduction = ($totalDeductionN + $tax);

						$netPay = ($basicPay - $totalDeduction);

						$payroll[$get->employee_id]['eid'] = $get->eid;
						$payroll[$get->employee_id]['fullname'] = $get->firstname." ".$get->lastname;
						$payroll[$get->employee_id]['NetPay'] = $netPay;
						$payroll[$get->employee_id]['basicPay'] = $basicPay;
						$payroll[$get->employee_id]['sss'] = $sss;
						$payroll[$get->employee_id]['pagibig'] = $pagibig;
						$payroll[$get->employee_id]['philHealth'] = $philHealth;
						$payroll[$get->employee_id]['tax'] = $tax;
						$payroll[$get->employee_id]['lates'] = $lates;
						$payroll[$get->employee_id]['totalDeduction'] = $totalDeduction;

						$isUPdate++;
						//$dtrsummary = $this->crud->read("UPDATE _t",array(":StartDate"=>$StartDate,":EndDate"=>$EndDate),'obj');
				


					}
					$data['success'] = '<div class="alert alert-success" style="margin-top: 5px;margin-bottom: 5px;" data-fade="3000">Daily Time Summary was successfully submitted.<button type="button" class="close fade" data-dismiss="alert">&times;</button></div>';
						if ($isUPdate > 0) {
							$aResult['cutoff'] = $_POST['cutoff'];
							$aResult['date_start'] = $StartDate;
							$aResult['date_end'] = $EndDate;
							$payslip_id = $this->crud->create("_tpayslips",$aResult);
							foreach ($dtrsummaryx as $get) {
								$result['payroll_status'] = 1;
								$result['payslip_id'] = $payslip_id;
								$isupdate = $this->crud->update('_tdailytimerecord',$result,array('dailytimerecord_id'=>$get->dailytimerecord_id));
			
							}
						
						} 
					//$data['success'] = "";
					//print_r($payroll);
					$data['payroll'] = $payroll;
					$this->template->_admin('xadmin/payroll',$data,$this->load);

		}else{
			redirect("xadmin/payroll");
		}
	}
	
	public function upload(){

		//	header('Content-type: application/json');
			$valid_exts = array('xls', 'xlsx'); // valid extensions
			$max_size = 200 * 1024; // max file size (200kb)
			$path = 'uploads/'; // upload directory
			$current_date = strtotime(date('Y-m-d'));
			//echo $_SERVER['REQUEST_METHOD'];
			if ( $_SERVER['REQUEST_METHOD'] === 'POST' )
			{
				if( @is_uploaded_file($_FILES['myfile']['tmp_name']) )
				{
					// get uploaded file extension
					$ext = strtolower(pathinfo($_FILES['myfile']['name'], PATHINFO_EXTENSION));
					// looking for format and size validity
					if (in_array($ext, $valid_exts) AND $_FILES['myfile']['size'] < $max_size)
					{
						// unique file path
						$path = $path . uniqid(). '.' .$ext;
						// move uploaded file from temp to uploads directory
						if (move_uploaded_file($_FILES['myfile']['tmp_name'], $path))
						{
							//$status = 'Image successfully uploaded!';
							//$status = 1;
							set_include_path(get_include_path() . PATH_SEPARATOR . 'vendor/');
							include 'PHPExcel/IOFactory.php';
							$inputFileName = $path;
							try {
								$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
							} catch(Exception $e) {
								header("Location: ".INDEX_PAGE."payslip-m&a=import&mode=import&success=mismatch");
								die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
							}
							
							$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
							//print_r($sheetData);
							$this->load->libraries(array('myDate'));
							$uploadResult = 0;
							if(count($sheetData)>=6){
								$possible_trim = array("DATE:","DATE: ");
								$date = str_replace($possible_trim,'',$sheetData[3]['A']);
								$date = date("Y-m-d",strtotime(trim($date)));
								$totalHrs = 0;
								$minutes = 0;
								
								for($i = 6;$i <= count($sheetData);$i++){
									$eid = $sheetData[$i]['B'];
									$timein_morning = $timein1 = ($sheetData[$i]['C']!="") ? $date." ".$sheetData[$i]['C'].":00" : null;
									$timeout_morning = $timeout1 = ($sheetData[$i]['D']!="") ? $date." ".$sheetData[$i]['D'].":00" : null;
									$timein_afternoon = $timein2 = ($sheetData[$i]['E']!="") ?  $date." ".$sheetData[$i]['E'].":00" : null;
									$timeout_afternoon = $timeout2 =  ($sheetData[$i]['F']!="") ?  $date." ".$sheetData[$i]['F'].":00" : null;
						
										$emp_record = $this->crud->read("SELECT * FROM _temployee WHERE eid=:id AND status =1",array(":id"=>$eid),'assoc');
											if ($emp_record) {
												$employee_id = $emp_record['employee_id'];   	
												
												$dtr_record = $this->crud->read("SELECT count(*) AS count FROM _tdailytimerecord WHERE employee_id=:employee_id AND _date=:dtrdate",array(":employee_id"=>$employee_id,":dtrdate"=>$date),'assoc');
												
													if ($dtr_record['count'] <= 0) {
														
														$timeLimitEnd =  strtotime($date." 08:15:00");
														
														 $morning =  strtotime($timein_morning);
															$timeLimitStart = strtotime($date." 08:00:00");

															$morningTimeEnd = strtotime($date." 12:00:00");
															$morningEnd = strtotime($timeout_morning);
																
																if ($timein_morning!="" && $morning <= $timeLimitEnd && $morning >= $timeLimitStart) {
																	$timein_morning = date("Y-m-d H:i:s",$timeLimitStart);
																}

																if ($timeout_morning!="" && $morningEnd > $morningTimeEnd) {
																	$timeout_morning = date("Y-m-d H:i:s",$morningTimeEnd);
																}
																$morningHrs = 0;
																$morningMns = 0;
																if ($timein_morning!=null && $timeout_morning != null) {
																
																	$morningTotal = $this->myDate->diff($timein_morning,$timeout_morning);
																	$morningHrs = $morningTotal['hrs'];
																	$morningMns = $morningTotal['mins'];

																	$result['timein_morning'] = $timein1;
																	$result['timeout_morning'] = $timeout1;
																	}

																$afternoonTimeStart = strtotime($date." 13:00:00");
																$afternoonTimeEnd = strtotime($date." 17:00:00");
																$afternoon = strtotime($timein_afternoon);
																	if ($timein_afternoon!="" && $afternoon <= $afternoonTimeStart) {
																		$timein_afternoon = date("Y-m-d",$afternoon);
																	}

																	if ($timeout_afternoon!="" && $afternoon >= $afternoonTimeEnd) {
																		$timeout_afternoon = date("Y-m-d",$afternoonTimeEnd);
																	}

																$afternoonHrs = 0;
																$afternoonMns = 0;
																	if ($timein_afternoon !=null && $timeout_afternoon != null) {
																		
																		$afternoonTotal = $this->myDate->diff($timein_afternoon,$timeout_afternoon);
																		$afternoonHrs = $afternoonTotal['hrs'];
																		$afternoonMns = $afternoonTotal['mins'];
																		$result['timein_afternoon'] = $timein2;
																		$result['timeout_afternoon'] = $timeout2;

																	}
																$totalMinutes = $morningMns + $afternoonMns;
																$hours = intval($totalMinutes/60);
																$minutes = $totalMinutes - ($hours * 60);
																$totalHrs = ($morningHrs + $afternoonHrs + $hours);
																$overallHrs = $totalHrs;
																$overmins = $minutes;
																	if($totalHrs >= 8){
																		$totalHrs = 8;
																		$minutes = 0;
																	}

																	


																	$result['overallHrs'] = $overallHrs;
																	$result['over_mins'] = $overmins;
																	$result['hrs'] = $totalHrs;
																	$result['mins'] = $minutes;
																	$result['eid'] = $eid;
																	$result['employee_id'] = $employee_id;
																	$result['_date'] = $date;

																	$uploadResult += $this->crud->create("_tdailytimerecord",$result);
																	
													}

											}
									
								}
							
								if ($uploadResult > 0) {
									$status = "Ok";
								}
								
							}else{
								$status = "File is empty";
							}
						}
						else {
							//$status = 'Upload Fail: Unknown error occurred!';
							$status = 2;
						}
					}
					else {
						//$status = 'Upload Fail: Unsupported file format or It is too large to upload!';
						$status = 3;
					}
				}
				else {
					//$status = 'Upload Fail: File not uploaded!';
					$status = 4;
				}
			}
			else {
				//$status = 'Bad request!';
				$status = 5;
			}

			// echo out json encoded status
	//echo json_encode(array('status' => $status));
	echo $status;
	
	}
	

	
}