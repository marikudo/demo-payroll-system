<?php

class daily_attendance extends crackerjack{
	
	public function __construct(){
		parent::__construct();
		if ($this->session->_get('xadminlogin')==false) { redirect('xadmin/home/auth');}
		
	}
	public function index(){
		if($this->session->_get('message')==1){
			if($this->session->_get('action')=='update'){
				$data['success'] = '<div class="alert alert-success" style="margin-top: 5px;margin-bottom: 5px;" data-fade="3000">Position was successfully updated.<button type="button" class="close fade" data-dismiss="alert">&times;</button></div>';
			}
			if($this->session->_get('action')=='add'){
				$data['success'] = '<div class="alert alert-success" style="margin-top: 5px;margin-bottom: 5px;" data-fade="3000">Position was successfully added.<button type="button" class="close fade" data-dismiss="alert">&times;</button></div>';
			}
		$this->session->_set(array('message'=>false,'action'=>''));
		}
		//$data['position'] = $this->crud->read('SELECT p.*,d.department FROM _position AS p LEFT JOIN _department AS d ON p.department_id = d.department_id',array(),'obj');
		$data['dtr'] =$x= $this->crud->read("SELECT * FROM _tdailytimerecord AS dtr INNER JOIN _temployee AS e ON dtr.eid = e.eid ORDER BY dtr._date DESC",array(),'obj');
		//	print_pre($x);
		$this->template->_admin('xadmin/dtr',$data,$this->load);
		//$this->template->adminTemplate('xadmin/dtr',$data,$this->load);
	}
	
	
	public function add($id = false){
		
		$this->load->libraries(array('form'));
		$result = $this->form->post('btn-submit');
		unset($result['position_id']);
			
		if($result){
		 if( $this->crud->create('_position',$result)){
			$this->session->_set(array('message'=>true,'action'=>'add'));
			redirect('xadmin/position/index/success');
		} 
		}
	$data['department'] = $this->crud->read('SELECT * FROM _department WHERE status=1',array(),'obj');
	$data['action'] = 'Add';
			$this->template->adminTemplate('xadmin/position_',$data,$this->load);
	}

	public function edit($id =false){
		$this->load->libraries(array('form'));
			
		$result = $this->form->post('btn-submit');
		
			if ($result) {
				# code...
				$position_id = $result['position_id'];
				unset($result['position_id']);
				
				
				 $isupdate = $this->crud->update('_position',$result,array('position_id'=>$position_id));
					if ($isupdate==true) {
						$this->session->_set(array('message'=>true,'action'=>'update'));
						redirect('xadmin/position/index/success');
					} 
			}

			
			$this->hash->hash_encryption($id[0]);
			$id = $this->hash->decrypt(str_replace('_', '/', $id[1]));
			$data['result'] = $this->crud->read('SELECT * FROM _position WHERE position_id = :id',array(':id'=>$id),'assoc');
			$data['action'] = 'Edit';
			$data['department'] = $this->crud->read('SELECT * FROM _department WHERE status=1',array(),'obj');
			$this->template->adminTemplate('xadmin/position_',$data,$this->load);

	}

	public function doesexists($data){
		$mode = $data[0];
		$a = "SELECT count(*) as count FROM _position WHERE department_id =:id AND position=:val LIMIT 0,1";
		 $res =  $this->crud->read($a,array(':id'=>$_REQUEST['department_id'],':val'=>$_REQUEST['position']),'assoc');
			$result = 'true';
				if ($res['count'] > 0) {
					$result = 'false';
					if($_REQUEST['current']==$_REQUEST['position'] && $_REQUEST['department']==$_REQUEST['department_id'] && $mode=='edit'){
								$result = 'true';
					}
				}				
			echo $result;
			
	}
	
	public function upload(){

		//	header('Content-type: application/json');
			$valid_exts = array('xls', 'xlsx'); // valid extensions
			$max_size = 200 * 1024; // max file size (200kb)
			$path = 'uploads/'; // upload directory
			$current_date = strtotime(date('Y-m-d'));
			//echo $_SERVER['REQUEST_METHOD'];
			if ( $_SERVER['REQUEST_METHOD'] === 'POST' )
			{
				if( @is_uploaded_file($_FILES['myfile']['tmp_name']) )
				{
					// get uploaded file extension
					$ext = strtolower(pathinfo($_FILES['myfile']['name'], PATHINFO_EXTENSION));
					// looking for format and size validity
					if (in_array($ext, $valid_exts) AND $_FILES['myfile']['size'] < $max_size)
					{
						// unique file path
						$path = $path . uniqid(). '.' .$ext;
						// move uploaded file from temp to uploads directory
						if (move_uploaded_file($_FILES['myfile']['tmp_name'], $path))
						{
							//$status = 'Image successfully uploaded!';
							//$status = 1;
							set_include_path(get_include_path() . PATH_SEPARATOR . 'vendor/');
							include 'PHPExcel/IOFactory.php';
							$inputFileName = $path;
							try {
								$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
							} catch(Exception $e) {
								header("Location: ".INDEX_PAGE."payslip-m&a=import&mode=import&success=mismatch");
								die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
							}
							
							$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
							//print_r($sheetData);
							$this->load->libraries(array('myDate'));
							$uploadResult = 0;
							if(count($sheetData)>=6){
								$possible_trim = array("DATE:","DATE: ");
								$date = str_replace($possible_trim,'',$sheetData[3]['A']);
								$date = date("Y-m-d",strtotime(trim($date)));
								$totalHrs = 0;
								$minutes = 0;
								
								for($i = 6;$i <= count($sheetData);$i++){
									$eid = $sheetData[$i]['B'];
									$timein_morning = $timein1 = ($sheetData[$i]['C']!="") ? $date." ".$sheetData[$i]['C'].":00" : null;
									$timeout_morning = $timeout1 = ($sheetData[$i]['D']!="") ? $date." ".$sheetData[$i]['D'].":00" : null;
									$timein_afternoon = $timein2 = ($sheetData[$i]['E']!="") ?  $date." ".$sheetData[$i]['E'].":00" : null;
									$timeout_afternoon = $timeout2 =  ($sheetData[$i]['F']!="") ?  $date." ".$sheetData[$i]['F'].":00" : null;
						
										$emp_record = $this->crud->read("SELECT * FROM _temployee WHERE eid=:id AND status =1",array(":id"=>$eid),'assoc');
											if ($emp_record) {
												$employee_id = $emp_record['employee_id'];   	
												
												$dtr_record = $this->crud->read("SELECT count(*) AS count FROM _tdailytimerecord WHERE employee_id=:employee_id AND _date=:dtrdate",array(":employee_id"=>$employee_id,":dtrdate"=>$date),'assoc');
												
													if ($dtr_record['count'] <= 0) {
														
														$timeLimitEnd =  strtotime($date." 08:15:00");
														
														 $morning =  strtotime($timein_morning);
															$timeLimitStart = strtotime($date." 08:00:00");

															$morningTimeEnd = strtotime($date." 12:00:00");
															$morningEnd = strtotime($timeout_morning);
																
																if ($timein_morning!="" && $morning <= $timeLimitEnd && $morning >= $timeLimitStart) {
																	$timein_morning = date("Y-m-d H:i:s",$timeLimitStart);
																}

																if ($timeout_morning!="" && $morningEnd > $morningTimeEnd) {
																	$timeout_morning = date("Y-m-d H:i:s",$morningTimeEnd);
																}
																$morningHrs = 0;
																$morningMns = 0;
																if ($timein_morning!=null && $timeout_morning != null) {
																
																	$morningTotal = $this->myDate->diff($timein_morning,$timeout_morning);
																	$morningHrs = $morningTotal['hrs'];
																	$morningMns = $morningTotal['mins'];

																	$result['timein_morning'] = $timein1;
																	$result['timeout_morning'] = $timeout1;
																	}

																$afternoonTimeStart = strtotime($date." 13:00:00");
																$afternoonTimeEnd = strtotime($date." 17:00:00");
																$afternoon = strtotime($timein_afternoon);
																	if ($timein_afternoon!="" && $afternoon <= $afternoonTimeStart) {
																		$timein_afternoon = date("Y-m-d",$afternoon);
																	}

																	if ($timeout_afternoon!="" && $afternoon >= $afternoonTimeEnd) {
																		$timeout_afternoon = date("Y-m-d",$afternoonTimeEnd);
																	}

																$afternoonHrs = 0;
																$afternoonMns = 0;
																	if ($timein_afternoon !=null && $timeout_afternoon != null) {
																		
																		$afternoonTotal = $this->myDate->diff($timein_afternoon,$timeout_afternoon);
																		$afternoonHrs = $afternoonTotal['hrs'];
																		$afternoonMns = $afternoonTotal['mins'];
																		$result['timein_afternoon'] = $timein2;
																		$result['timeout_afternoon'] = $timeout2;

																	}
																$totalMinutes = $morningMns + $afternoonMns;
																$hours = intval($totalMinutes/60);
																$minutes = $totalMinutes - ($hours * 60);
																$totalHrs = ($morningHrs + $afternoonHrs + $hours);
																$overallHrs = $totalHrs;
																$overmins = $minutes;
																	if($totalHrs >= 8){
																		$totalHrs = 8;
																		$minutes = 0;
																	}

																	


																	$result['overallHrs'] = $overallHrs;
																	$result['over_mins'] = $overmins;
																	$result['hrs'] = $totalHrs;
																	$result['mins'] = $minutes;
																	$result['eid'] = $eid;
																	$result['employee_id'] = $employee_id;
																	$result['_date'] = $date;

																	$uploadResult += $this->crud->create("_tdailytimerecord",$result);
																	
													}

											}
									
								}
							
								if ($uploadResult > 0) {
									$status = "Ok";
								}
								
							}else{
								$status = "File is empty";
							}
						}
						else {
							//$status = 'Upload Fail: Unknown error occurred!';
							$status = 2;
						}
					}
					else {
						//$status = 'Upload Fail: Unsupported file format or It is too large to upload!';
						$status = 3;
					}
				}
				else {
					//$status = 'Upload Fail: File not uploaded!';
					$status = 4;
				}
			}
			else {
				//$status = 'Bad request!';
				$status = 5;
			}

			// echo out json encoded status
	//echo json_encode(array('status' => $status));
	echo $status;
	
	}
	

	
}