<?php
class current_time extends crackerjack{
		public function __construct(){
			parent::__construct();
		}

		public function index(){
			$this->load->libraries(array('Array2xml'));
			$data = array();
			$data['current_time'] = date("F d, Y H:i:s", time());
			header('Content-Type: application/xml; charset=utf-8');
			/*echo '<?xml version="1.0" encoding="UTF-8" ?>';*/
			print_r($this->Array2xml->convert($data));
		}
}