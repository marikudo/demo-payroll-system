<fieldset class="title-container">
<legend><i class="fa fa-user"></i> <?=ucwords($user['permission']['module'])?> Details</legend>
<div class="res-container-x">
<form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data" id="validate-form">
 <fieldset class="fieldset-group">
     <div class="form-group">
          <label class="col-sm-3 control-label ckey">
		  <?php
              $avatar = ($result['avatar']=="") ? base_url()."media/images/no_avatar.jpg" : base_url()."uploads/avatar/".$result['avatar'];
              $avatarx = ($result['avatar']=="") ? "no_avatar.jpg" : $result['avatar'];
            ?>
				<img src="<?=$avatar?>" id="img_preview" class="img-thumbnail" style="width:100px;margin-right:10px" /></label>
				<div class="col-sm-7">
					<fieldset class="fieldset-group">
					
					<h3><strong><?=ucfirst($result['firstname'])?> <?=ucfirst($result['middlename'][0])?>. <?=ucfirst($result['lastname'])?></strong></h3>
					<h4>[<?=$result['eid']?>] <?=$result['position']?> - <?=$result['department']?></h4>
					<div class="alert alert-info">Fingerprint Registration page has been generated.
            <strong><?=$path?> : </strong>
            Please open with Internet Explorer to run for some features.<strong>Recommended</strong></div>
					
				</div>
        </div>
	
	

  <br class="clear"/>
    </fieldset>

    <div class="form-actions" style="padding-left:20px">
       <a href="javascript:history.back()" class="btn btn-default">Back</a>
	   </div>

    

</form>
</div>
</fieldset>

<style type="text/css">
.form-horizontal .control-label, .form-horizontal .radio, .form-horizontal .checkbox, .form-horizontal .radio-inline, .form-horizontal .checkbox-inline{
	padding-top:0px
}
</style>

