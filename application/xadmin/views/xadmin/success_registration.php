<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.png">

    <title>Thesis Payroll System</title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/sticky-footer.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?=base_url()?>assets/css/main.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
<div class="header">
      <div class="container">
      <div class="page-header clearfix" style="margin-top:0px;margin-bottom:0px">
        <img src="<?=base_url()?>assets/employee/images/logo.jpg" class="pull-left" />
        <h1 class="pull-left">St. Bernadatte Publishing House Corporation <br /> <small>" Quest for Excellence in Educating Young Minds"</small></h1>
      </div>
    </div>
</div>
      <div class="container">
        <div class="row cleasfix">
            <div class="col-sm-6" style="float:none;margin:0 auto;margin-top:20px">

<fieldset class="fieldset-group">
     <div class="form-group">
        <h2 class="alert alert-success">You are successfully registered.</h2>
		  		 <label class="col-sm-3 control-label ckey">
		  <?php
              $avatar = ($result['avatar']=="") ? base_url()."media/images/no_avatar.jpg" : base_url()."uploads/avatar/".$result['avatar'];
              $avatarx = ($result['avatar']=="") ? "no_avatar.jpg" : $result['avatar'];
            ?>
				<img src="<?=$avatar?>" id="img_preview" class="img-thumbnail" style="margin-right:10px" /></label>
				<div class="col-sm-7">
					<fieldset class="fieldset-group">
						<legend style="padding: 0;margin-bottom: 0;">Basic Information</legend>
					</fieldset>
					<h3><strong><?=ucfirst($result['firstname'])?> <?=ucfirst($result['middlename'][0])?>. <?=ucfirst($result['lastname'])?></strong></h3>
					<h4>[<?=$result['eid']?>] <?=$result['position']?> - <?=$result['department']?></h4>
					<p><strong>Hired Date</strong> : <?=date("F j, Y",strtotime($result['hire_date']))?></p>
					<p><strong>Address</strong> : <?=$result['address1']." ".$result['address2']." ".$result['address3']?></p>
					<p><strong>Mobile #</strong> : <?=$result['mobile_number']?></p>
        </div>
        </div>
	
	

  <br class="clear">
    </fieldset>
    </div>
    </div>
    </div>
    <div class="footer">
      <div class="container">
	  
        <p class="text-center"> &copy; <?=date("Y")?>Web-based Payroll System. All rights reserved.</p>
      </div>
    </div>
    </body>
    </html>