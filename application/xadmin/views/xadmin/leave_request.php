<fieldset class="title-container">
<legend><i class="fa fa-user"></i> <?=ucwords($user['permission']['module'])?></legend>
<input type="hidden" id="department_id" value="<?=$user['permission']['department_id']?>"/>
<input type="hidden" id="data" />
<?=isset($success) ? showMessage($success) : null;?>
<div id="xrole">
<table class="table table-hover table-custom display" style="font: 12px 'Arial';" id="table">
		<thead>
			<tr>
				
        <th class="align-center">Action</th>
        <th class="align-center">EID</th>
        <th class="align-center">First Name</th>
        <th class="align-center">Last Name</th>
        <th class="align-center">Request</th>
        <th class="align-center">Date From</th>
        <th class="align-center">Date To</th>
        <th class="align-center">Date Requested</th>
        <th class="align-center">Status</th>
			</tr>
        </thead>
		<tbody>
		</tbody>
	</table>
	</div>
</fieldset>

<style type="text/css">
.row{margin-left: 0px;margin-right: 0px}
</style>
<script type="text/javascript" src="<?=base_url()?>media/js/jquery-gridTools.js"></script>
<script type="text/javascript">
var data = [];
$(document).ready(function(){
		/* Datatable decleration
		-----------------------------*/
	 var oTable =  $('#table').dataTable({
	 	"sDom":"T<'clear'>rtip<'clear'>",
		"bProcessing": true,	
		"bServerSide": true,
		"sAjaxSource": "<?=base_url()?>xadmin/api/data/?gConf=<?=$hashConfig?>",
    
		"aoColumns":[

                {"bSearchable":false,"mData":"button","sWidth":"50px","sClass":'align-center'},
                {"bSearchable":false,"mData":"eid","sWidth":"50px","sClass":'align-center'},
                {"bSearchable":true,"mData":"firstname","sWidth":"120px","sClass":'align-center'},
                {"bSearchable":true,"mData":"lastname","sWidth":"120px","sClass":'align-center'},
                {"bSearchable":false,"mData":"leavesettings_id","sWidth":"120px","sClass":'align-center'},
                {"bSearchable":false,"mData":"date_from","sWidth":"90px","sClass":'align-center'},
                {"bSearchable":false,"mData":"date_to","sWidth":"130px","sClass":'align-center'},
                {"bSearchable":false,"mData":"created_at","sWidth":"170px","sClass":'align-right'},
                {"bSearchable":false,"mData":"request_status","sWidth":"50px","sClass":'align-center'}
                ],
    "aoColumnDefs":[
                  {'bSortable':false,'aTargets':[0]},
                  {'bSortable':false,'aTargets':[1]},
                  {'bSortable':false,'aTargets':[2]},
                  {'bSortable':true,'aTargets':[3]},
                  {'bSortable':true,'aTargets':[4]},
                  {'bSortable':true,'aTargets':[5]},
                  {'bSortable':true,'aTargets':[6]},
                  {'bSortable':false,'aTargets':[7]},
                  {'bSortable':false,'aTargets':[8]},
                  ],
      "order": [[ 2, "desc" ]],
        "oTableTools": {
              "sRowSelect": "",
                 "aButtons": [
                              {
                                "sExtends": "refreshBtn",
                              },

                             /* <?php
                                if ($user['permission']['_create']==1) {
                                  ?>
                                         {
                                          "sExtends": "addBtn",
                                          "sButtonText":"<i class='fa fa-plus white'></i> New record",
                                          "sUrl":"<?=base_url()?>xadmin/<?=$user['permission']['_url']?>/new-record"
                                        },
                                      <?php
                                    }
                                       ?>
                              <?php
                                    if ($user['permission']['_update']==1) {
                                  ?>
                                       {
                                          "sExtends": "bActivate",
                                          "dxConfig" : "<?=$hashConfig?>"
                                        },
                                        {
                                          "sExtends": "bDeactivate",
                                          "dxConfig" : "<?=$hashConfig?>"
                                        },
                                      <?php
                                    }
                                  ?>
                            */
                             ]
            },
           "oLanguage": {
            "sProcessing": "<img src='<?=base_url()?>media/images/loading.gif'> Processing..."
            },
            "sScrollY": "400px",
            "sScrollX": "",
            "bScrollCollapse": false,
            "iDisplayLength": 50,

    })
		/* end of datatable
		----------------------------------*/
 
 });



</script>