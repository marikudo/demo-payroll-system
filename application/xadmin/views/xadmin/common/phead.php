<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=base_url()?>media/js/jquery-2.1.0.min.js"></script>
    <script src="<?=base_url()?>media/js/nanoscroller/jquery.nanoscroller.js"></script>
    <title>Xadmin - <?=$title?></title>
    <!-- Bootstrap -->
    <link href="<?=base_url()?>media/css/bootstrap.css" rel="stylesheet">
    <link href="<?=base_url()?>media/css/font-awesome.css" rel="stylesheet">
    <link href="<?=base_url()?>media/css/xadmin.css" rel="stylesheet">
    <link href="<?=base_url()?>media/css/max768px.css" rel="stylesheet">

    <link href="<?=base_url()?>media/js/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?=base_url()?>media/js/bootstrap-modal/bootstrap-modal.css" rel="stylesheet">
    <link href="<?=base_url()?>media/js/bootstrap-modal/bootstrap-modal-bs3patch.css" rel="stylesheet">
    <link href="<?=base_url()?>media/js/datatables/datatables.css" rel="stylesheet">
    <link href="<?=base_url()?>media/js/nanoscroller/nanoscroller.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="<?=base_url()?>media_be/js/html5shiv.js"></script>
      <script src="<?=base_url()?>media_be/js/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .xconBar{
          margin-top: 0px;
          margin-right: 0px;
        }
    </style>
    <script type="text/javascript">
    
    </script>
  </head>
  <body>