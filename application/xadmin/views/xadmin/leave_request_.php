<fieldset class="title-container">
<legend><i class="fa fa-user"></i> <?=ucwords($user['permission']['module'])?></legend>
<div class="res-container">
<form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data" id="validate-form">
<input type="hidden" name="emprequest" value="<?=$result['emprequest']?>" />
  <div class="form-group">
    <label class="col-sm-3 control-label ckey">Employee Fullname :</label>
    <div class="col-sm-5">
        <?=$result['lastname'].", ".$result['firstname']?>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label ckey">Request :</label>
    <div class="col-sm-5">
      <?php

      $request = '';
        switch ($result['leavesettings_id']) {
          case 1:
           $request = 'Vacation Leave';
          break;
           case 2:
           $request = 'Emergency Leave';
          break;
           case 3:
           $request = 'Sick Leave';
          break;
           case 4:
           $request = 'Overtime';
          break;
          
          default:
            # code...
            break;
        }

      ?>
        <?=$request?>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label ckey"> Date Requested : </label>
    <div class="col-sm-6">
      <?="<strong>From:</strong> ".$result['date_from']." <br /><strong>To: </strong>".$result['date_to']?>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label ckey"> Number of Days : </label>
    <div class="col-sm-6">
      <?php


      $d1 = strtotime($result['date_from']);
            $d2 = strtotime($result['date_to']);
              $diff   = $d2 - $d1;
              $no_of_nights   = floor($diff/(60*60*24));
             echo  $no_of_days   = ($no_of_nights + 1)." Day(s)";
      ?>
    </div>
  </div>
    <div class="form-group">
    <label class="col-sm-3 control-label ckey">Reason : </label>
    <div class="col-sm-6">
      <?=$result['reason']?>  
    </div>
  </div>
    <?php
    if($action=='View'){
            ?>
                <div class="form-group">
                  <label class="col-sm-3 control-label ckey">Request Status : </label>
                  <div class="col-sm-6">
                    <?php
                    $ab = "<span class='badge badge-warning'>Pending</span>";
                      switch ($result['request_status']) {
                        case 1:
                           $ab = "<span class='badge badge-warning'>Approved</span>";
                          break;
                        case 2:
                           $ab = "<span class='badge badge-warning'>Disapproved</span>";
                          break;
                        
                        default:
                          # code...
                          break;
                      }
                      echo $ab;
                    ?>  
                  </div>
                </div>
            <?php
          }

    ?>

      <div class="form-group">
    <label class="col-sm-3 control-label ckey"></label>
    <div class="col-sm-6">
       <?php
          if ($action=='Edit') {
            ?>
      
      <?php
          }else{
     //       echo $result['remarks'];
          }
      ?>
    </div>
  </div>

    <div class="form-actions" style="padding-left:20px">
      <?php
          if ($action=='Edit') {
            ?>


             <input type="submit" class="btn btn-danger" name="btn_disapproved" value="Denied">
             <input type="submit" class="btn btn-success blue" name="btn_success" value="Approved">
       
            <?php
          }
      ?>
      <a href="javascript:history.back()" class="btn btn-warning">Back</a>
	   </div>
</form>
</div>
</fieldset>
<script type="text/javascript">
$(document).ready(function(){
  $('.number').numeric();
  $('.alphanumeric-n').alpha({allow:". "});
  $('.alphanumeric').alphanumeric({allow:"., -"});
  $('.alphanumeric-d').alphanumeric({allow:"-"});
  var validator = $("#validate-form").validate({
    rules: {
      department_id:{
        required:true,
      },
       position:{
        required:true,
      }
    
    },messages:{ 
      department_id:{
        //remote: $.format("<strong>{0}</strong> is already exists.")
      }
    },
    
    errorPlacement: function(error, element) {
      if ( element.is(":radio") )
        error.appendTo( element.parent().next().next() );
      else if ( element.is(":checkbox") )
        error.appendTo ( element.next() );
      else
        error.appendTo( element.parent().find('span.validation-status') );
    },
    success: "valid",
    submitHandler: function(form){
      $('button[type=submit]').attr('disabled', 'true');
      $(this).bind("keypress", function(e) { if (e.keyCode == 13) return false; });
      form.submit(form);
    }
  });
});

</script>
						
						
				
	



