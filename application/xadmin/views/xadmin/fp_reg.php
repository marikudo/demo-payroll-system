<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.png">

    <title>Thesis Payroll System</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/sticky-footer.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">
    <script type="text/javascript" src="loadxmldoc.js"></script>

<script lang=javascript>
<!--
var aaa = 0;
function fnOpenDevice()
{
	for ( i=0; i<4; i++)
	{
		document.frmmain.objFP[i].DeviceID = document.frmmain.inc.value;
		document.frmmain.objFP[i].CodeName = document.frmmain.dev.value;
		document.frmmain.objFP[i].MinutiaeMode = document.frmmain.templateFormat.value;
	}

	// template format of objVerify should be the same to those of objFP[]
	objVerify.MinutiaeMode = document.frmmain.templateFormat.value;
	return;
}

function fnCapture(idx)
{
	document.frmmain.objFP[idx].Capture();
	var result = document.frmmain.objFP[idx].ErrorCode;
	if (result == 0)
	{	
		//var strimg1 = objFP.ImageTextData;
	
		var strmin = document.frmmain.objFP[idx].MinTextData;
		document.frmmain.min.value = strmin;
	}
	else
		alert('failed - ' + result);

	return;
}

function fnCapture2(idx)
{
	document.frmmain.objFP[1].Capture();
	var result = document.frmmain.objFP[1].ErrorCode;
	if (result == 0)
	{	
		//var strimg1 = objFP.ImageTextData;
	
		var strmin = document.frmmain.objFP[1].MinTextData;
		document.frmmain.min2.value = strmin;

	}
	else
		alert('failed - ' + result);

	return;
}

function fnCapture3(idx)
{
	document.frmmain.objFP[2].Capture();
	var result = document.frmmain.objFP[2].ErrorCode;
	if (result == 0)
	{	
		//var strimg1 = objFP.ImageTextData;
	
		var strmin = document.frmmain.objFP[2].MinTextData;
		document.frmmain.min3.value = strmin;

	}
	else
		alert('failed - ' + result);

	return;
}

function fnCapture4(idx)
{
	document.frmmain.objFP[3].Capture();
	var result = document.frmmain.objFP[3].ErrorCode;
	if (result == 0)
	{	
		//var strimg1 = objFP.ImageTextData;
	
		var strmin = document.frmmain.objFP[3].MinTextData;
		document.frmmain.min4.value = strmin;

	}
	else
		alert('failed - ' + result);

	return;
}

function fnVerify()
{	
	var strmin1 = document.frmmain.min.value;
	var strmin2 = document.frmmain.min2.value;

	if ( objVerify.VerifyForText(strmin1, strmin2) && objVerify.ErrorCode == 0 ){
		document.getElementById("sub3").style.display = 'inline';
	if ( objVerify.VerifyForText(strmin1, strmin2) && objVerify.ErrorCode == 0 ){
		document.getElementById("sub2").style.display = 'none';
		}
	}else
		alert('Failed - ' + objVerify.ErrorCode);
		
	return;
}


function fnSetimage()
{
	document.frmmain.objFP.ImageTextData = document.frmmain.img1.value;
	return;
}
// -->
</script>
<SCRIPT LANGUAGE="JavaScript1.2">

function myAgeValidation() {

    var lre = /^\s*/;
    var datemsg = "";
   
    var inputDate = document.frmmain.birthday.value;
    inputDate = inputDate.replace(lre, "");
    document.frmmain.birthday.value = inputDate;
    datemsg = isValidDate(inputDate);
        if (datemsg != "") {
            alert(datemsg);
            return;
        }
        else {
            //Now find the Age based on the Birth Date
            getAge(new Date(inputDate));
        }

}

function getAge(birth) {

    var today = new Date();
    var nowyear = today.getFullYear();
    var nowmonth = today.getMonth();
    var nowday = today.getDate();

    var birthyear = birth.getFullYear();
    var birthmonth = birth.getMonth();
    var birthday = birth.getDate();

    var age = nowyear - birthyear;
    var age_month = nowmonth - birthmonth;
    var age_day = nowday - birthday;
   
    if(age_month < 0 || (age_month == 0 && age_day <0)) {
            age = parseInt(age) -1;
        }
    //alert('You are ' + age + ' Years Old!');
   
    if ((age == 18 && age_month <= 0 && age_day <=0) || age < 18) {
	alert("Your Age is not Acceptable !");
    }
    else {
    document.frmmain.age.value = age;
    }

}

function isValidDate(dateStr) {

   
    var msg = "";
    // Checks for the following valid date formats:
    // MM/DD/YY   MM/DD/YYYY   MM-DD-YY   MM-DD-YYYY
    // Also separates date into month, day, and year variables

    // To require a 2 & 4 digit year entry, use this line instead:
    //var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{2}|\d{4})$/;
    // To require a 4 digit year entry, use this line instead:
    var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})$/;

    var matchArray = dateStr.match(datePat); // is the format ok?
    if (matchArray == null) {
        msg = "Date is not in a valid format.";
        return msg;
    }

    month = matchArray[1]; // parse date into variables
    day = matchArray[3];
    year = matchArray[4];

   
    if (month < 1 || month > 12) { // check month range
        msg = "Month must be between 1 and 12.";
        return msg;
    }

    if (day < 1 || day > 31) {
        msg = "Day must be between 1 and 31.";
        return msg;
    }

    if ((month==4 || month==6 || month==9 || month==11) && day==31) {
        msg = "Month "+month+" doesn't have 31 days!";
        return msg;
    }

    if (month == 2) { // check for february 29th
    var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
    if (day>29 || (day==29 && !isleap)) {
        msg = "February " + year + " doesn't have " + day + " days!";
        return msg;
    }
    }

    if (day.charAt(0) == '0') day= day.charAt(1);
   
    //Incase you need the value in CCYYMMDD format in your server program
    //msg = (parseInt(year,10) * 10000) + (parseInt(month,10) * 100) + parseInt(day,10);
   
    return msg;  // date is valid
}

</SCRIPT>
	<body onLoad='fnOpenDevice();'>
 	<div class="container header">
 		<div class="bio-container clearfix">
 			<div class="bio-wrapper">
 			<div class="page-header clearfix brand-details" style="margin-top:0px;margin-bottom:0px">
		        <img src="assets/employee/images/logo.jpg" class="pull-left brand-logo">
		        <h1 class="pull-left">St. Bernadatte Publishing House Corporation <br> <small>" Quest for Excellence in Educating Young Minds"</small></h1>
		      </div>
 			</div>
 		

	 		<div class="bio-body clearfix" style="clear:both">
	 			<div class="pull-left bio-capture" style="text-align:left">
	 			
							<?php
							  $avatar = ($a['avatar']=="") ? base_url()."media/images/no_avatar.jpg" : base_url()."uploads/avatar/".$a['avatar'];
							  $avatarx = ($a['avatar']=="") ? "no_avatar.jpg" : $a['avatar'];
							?>
							<center>
								<img src="<?=$avatar?>" id="img_preview" class="img-thumbnail" style="width:100px;margin-right:10px" /></label>
							</center>
			
							<h3><strong><?=ucfirst($a['firstname'])?> <?=ucfirst($a['middlename'][0])?>. <?=ucfirst($a['lastname'])?></strong></h3>
							<h4>[<?=$a['eid']?>] <?=$a['position']?> - <?=$a['department']?></h4>

							
	 			</div>
	 			<div class="pull-left bio-right-content">
	 				<h1>Welcome, <?=ucfirst($a['firstname'])?>!. Register your fingerprints.</h1>
	 				<OBJECT id=objVerify style="LEFT: 0px; TOP: 0px" height=0 width=0 
													classid="CLSID:8D613732-7D38-4664-A8B7-A24049B96117" 
													name=objVerify VIEWASTEXT>
													</OBJECT>
													
													<form action="<?=base_url()?>xadmin/register-fingerprint" name=frmmain method="POST">
													<input type=hidden name=dev value=2> 
													<input type=hidden name=inc value=-1> 
													<input type=hidden name=templateFormat value=512> 
													<input type="hidden" name="employee_id"  value="<?=$encrypt_id?>" />
												
													<ul id="registration">
														<li>
															<label class="inline">
																<OBJECT id=objFP style="LEFT: 0px; WIDTH: 149px; TOP: 0px; HEIGHT: 182px" height=182 
																width=149 classid="CLSID:D547FDD7-82F6-44e8-AFBA-7553ADCEE7C8" name=objFP VIEWASTEXT>
																<PARAM NAME="CodeName" VALUE="1">
																</OBJECT><br />

																<input class="btn btn-small btn-info no-radius" type=button name=btnCapture1 value='Capture' OnClick='fnCapture(0);'>
															</label>
															<label class="inline">
																<OBJECT id=objFP style="LEFT: 0px; WIDTH: 149px; TOP: 0px; HEIGHT: 182px" height=182 
																width=149 classid="CLSID:D547FDD7-82F6-44e8-AFBA-7553ADCEE7C8" name=objFP VIEWASTEXT>
																<PARAM NAME="CodeName" VALUE="1">
																</OBJECT><br />
																<input class="btn btn-small btn-info no-radius" type=button name=btnCapture2 value='Capture' OnClick='fnCapture2(1);'>
														
															</label>
															<label class="inline">
																<OBJECT id=objFP style="LEFT: 0px; WIDTH: 149px; TOP: 0px; HEIGHT: 182px" height=182 
																width=149 classid="CLSID:D547FDD7-82F6-44e8-AFBA-7553ADCEE7C8" name=objFP VIEWASTEXT>
																<PARAM NAME="CodeName" VALUE="1">
																</OBJECT><br />
																<input class="btn btn-small btn-info no-radius" type=button name=btnCapture3 value='Capture' OnClick='fnCapture3(0);'>
														
															</label>
															<label class="inline">
																<OBJECT id=objFP style="LEFT: 0px; WIDTH: 149px; TOP: 0px; HEIGHT: 182px" height=182 
																width=149 classid="CLSID:D547FDD7-82F6-44e8-AFBA-7553ADCEE7C8" name=objFP VIEWASTEXT>
																<PARAM NAME="CodeName" VALUE="1">
																</OBJECT><br />
																<input class="btn btn-small btn-info no-radius" type=button name=btnCapture4 value='Capture' OnClick='fnCapture4(0);'>
														
															</label>
														</li>
														<li>
																												
														<input type=hidden name="min">
														<input type=hidden name="min2">
														<input type=hidden name="min3">
														<input type=hidden name="min4">
														
														<style type="text/css">
														.div{
														display: none;
														}
														.div2{
														display: inline;
														}
														</style>
														<div class="div2" id="sub2">
														<label class="inline">
														<input class="btn btn-small btn-info no-radius" onClick='fnVerify();' type=button name="btnVerify" value='Verify' style='width:200px'>
														</label>
														</div>
														<div class="div" id="sub3">
														<label class="inline">
														<input class="btn btn-small btn-success no-radius" type=submit name="btnRegister" value='Register' style='width:200px'>
														</div>
														</li>
														</ul>
														</form>
	 			</div>
	 		</div>
	 		<div class="bio-footer">
	 			<p class="text-center"> &copy; 2015 Web-based Payroll System. All rights reserved.</p>
	 		</div>
 		</div>

 	</div>
 </body>


</html>