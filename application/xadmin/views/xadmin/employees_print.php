<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=base_url()?>media/js/jquery-2.1.0.min.js"></script>
    <script src="<?=base_url()?>media/js/nanoscroller/jquery.nanoscroller.js"></script>
    <title>Xadmin - <?=$title?></title>
    <!-- Bootstrap -->
    <link href="<?=base_url()?>media/css/bootstrap.css" rel="stylesheet">
    <link href="<?=base_url()?>media/css/font-awesome.css" rel="stylesheet">
    <link href="<?=base_url()?>media/css/xadmin.css" rel="stylesheet">
    <link href="<?=base_url()?>media/css/max768px.css" rel="stylesheet">

    <link href="<?=base_url()?>media/js/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?=base_url()?>media/js/bootstrap-modal/bootstrap-modal.css" rel="stylesheet">
    <link href="<?=base_url()?>media/js/bootstrap-modal/bootstrap-modal-bs3patch.css" rel="stylesheet">
    <link href="<?=base_url()?>media/js/datatables/datatables.css" rel="stylesheet">
    <link href="<?=base_url()?>media/js/nanoscroller/nanoscroller.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="<?=base_url()?>media_be/js/html5shiv.js"></script>
      <script src="<?=base_url()?>media_be/js/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .xconBar{
          margin-top: 0px;
          margin-right: 0px;
        }
    </style>
    <script type="text/javascript">
   
    </script>
  </head>
  <body>
<fieldset class="title-container">
<legend><i class="fa fa-user"></i> Employee Information</legend>
<div class="res-container-x">
<form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data" id="validate-form">
 <fieldset class="fieldset-group">
     <div class="form-group">
          <label class="col-sm-3 control-label ckey">
		  <?php
              $avatar = ($result['avatar']=="") ? base_url()."media/images/no_avatar.jpg" : base_url()."uploads/avatar/".$result['avatar'];
              $avatarx = ($result['avatar']=="") ? "no_avatar.jpg" : $result['avatar'];
            ?>
				<img src="<?=$avatar?>" id="img_preview" class="img-thumbnail" style="width:100px;margin-right:10px" /></label>
				<div class="col-sm-7">
					<fieldset class="fieldset-group">
						<legend style="padding: 0;margin-bottom: 0;">Basic Information</legend>
					</fieldset>
					<h3><strong><?=ucfirst($result['firstname'])?> <?=ucfirst($result['middlename'][0])?>. <?=ucfirst($result['lastname'])?></strong></h3>
					<h4>[<?=$result['eid']?>] <?=$result['position']?> - <?=$result['department']?></h4>
					<p><strong>Hired Date</strong> : <?=date("F j, Y",strtotime($result['hire_date']))?></p>
					<p><strong>Address</strong> : <?=$result['address1']." ".$result['address2']." ".$result['address3']?></p>
					<p><strong>Mobile #</strong> : <?=$result['mobile_number']?></p>
					<fieldset class="fieldset-group">
						<legend style="padding: 0;margin-bottom: 0;">Personal Information</legend>
					</fieldset>
				</div>
        </div>
	
	 <div class="form-group _s">
        <label class="col-sm-3 control-label ckey" style="">Social Security (SSS)</label>
        <div class="col-sm-7"><?=$result['_sss']?></div>
      </div>

      <div class="form-group _s">
        <label class="col-sm-3 control-label ckey">Philhealth</label>
        <div class="col-sm-7"><?=$result['_philhealth']?>
        </div>
      </div>

       <div class="form-group _s">
        <label class="col-sm-3 control-label ckey">Pagibig</label>
        <div class="col-sm-7"><?=$result['_pagibig']?></div>
      </div>

      <div class="form-group _s">
        <label class="col-sm-3 control-label ckey">TIN</label>
        <div class="col-sm-7"><?=$result['_tin']?> </div>
      </div>
	   <div class="form-group _s">
        <label class="col-sm-3 control-label ckey">Username</label>
        <div class="col-sm-7"><?=$result['email']?> </div>
      </div>

  <br class="clear"/>
    </fieldset>

    <div class="form-actions" style="padding-left:20px">
       <button type="button" onclick="window.print()" class="btn btn-primary blue"><span class="fa fa-print" style="color:#fff"></span> Print</button>
	     <a href="javascript:window.close()" class="btn btn-default">Close</a>
	   </div>

    

</form>
</div>
</fieldset>

<style type="text/css">
.form-horizontal .control-label, .form-horizontal .radio, .form-horizontal .checkbox, .form-horizontal .radio-inline, .form-horizontal .checkbox-inline{
	padding-top:0px
}
</style>
</body>
</html>
