<fieldset class="title-container">
<legend><i class="custom-icon-role"></i> Payroll</legend>
<input type="hidden" id="module" value="<?=$data['permission']['module_id']?>"/>
<input type="hidden" id="data" />
<?=(isset($success) ? $success :null);?>
<div class="clearfix" style="margin-bottom:5px;">
	<form action="<?=base_url()?>xadmin/payroll" method="POST">
	<div class="pull-left" style="margin-right:10px">
		<strong style="float: left;margin-right: 10PX;margin-top: 5PX;">SELECT DATE</strong>
		<select name="cutoff" class="form-control" style="width: 250px;float: left;">
			<?php
				$y = date("Y");
			?>
			<option value="12-31:15" <?=($cutoff=="12-31:15")? "selected" : null?>>DEC 31, <?=(date("Y")-1)?> - Jan 14, <?=$y?> </option>
			<option value="01-15:16" <?=($cutoff=="01-15:16")? "selected" : null?>>Jan 15, <?=$y?> - Jan 30, <?=$y?></option>

			<option value="01-31:15" <?=($cutoff=="01-31:15")? "selected" : null?>>Jan 31, <?=$y?> - Feb 14, <?=$y?></option>
			<option value="02-15:13" <?=($cutoff=="02-15:13")? "selected" : null?>>Feb 15, <?=$y?> - Feb 27, <?=$y?></option>

			<option value="02-28:15" <?=($cutoff=="02-28:15")? "selected" : null?>>Feb 28, <?=$y?> - Mar 14, <?=$y?></option>
			<option value="03-15:16" <?=($cutoff=="03-15:16")? "selected" : null?>>Mar 15, <?=$y?> - Mar 30, <?=$y?></option>

			<option value="03-30:15" <?=($cutoff=="03-30:15")? "selected" : null?>>Mar 30, <?=$y?> - Apr 14, <?=$y?></option>
			<option value="04-15:15" <?=($cutoff=="04-15:15")? "selected" : null?>>Apr 15, <?=$y?> - Apr 29, <?=$y?></option>

			<option value="04-30:15" <?=($cutoff=="04-30:15")? "selected" : null?>>Apr 30, <?=$y?> - May 14, <?=$y?></option>
			<option value="05-15:16" <?=($cutoff=="05-15:16")? "selected" : null?>>May 15, <?=$y?> - May 30, <?=$y?></option>

			<option value="05-31:15" <?=($cutoff=="05-31:15")? "selected" : null?>>May 31, <?=$y?> - Jun 14, <?=$y?></option>
			<option value="06-15:15" <?=($cutoff=="06-15:15")? "selected" : null?>>Jun 15, <?=$y?> - Jun 29, <?=$y?></option>

			<option value="06-30:15" <?=($cutoff=="06-30:15")? "selected" : null?>>Jun 30, <?=$y?> - Jul 14, <?=$y?></option>
			<option value="07-15:16" <?=($cutoff=="07-15:16")? "selected" : null?>>Jul 15, <?=$y?> - Jul 30, <?=$y?></option>

			<option value="07-31:15" <?=($cutoff=="07-31:15")? "selected" : null?>>Jul 31, <?=$y?> - Aug 14, <?=$y?></option>
			<option value="08-15:16" <?=($cutoff=="08-15:16")? "selected" : null?>>Aug 15, <?=$y?> - Aug 30, <?=$y?></option>

			<option value="08-31:15" <?=($cutoff=="08-31:15")? "selected" : null?>>Aug 31, <?=$y?> - Sep 14, <?=$y?></option>
			<option value="09-15:15" <?=($cutoff=="09-15:15")? "selected" : null?>>Sep 15, <?=$y?> - Sep 29, <?=$y?></option>

			<option value="09-30:15" <?=($cutoff=="09-30:15")? "selected" : null?>>Sep 30, <?=$y?> - >Oct 14, <?=$y?></option>
			<option value="10-15:16" <?=($cutoff=="10-15:16")? "selected" : null?>>Oct 15, <?=$y?> - Oct 30, <?=$y?></option>

			<option value="10-31:15" <?=($cutoff=="10-31:15")? "selected" : null?>>Oct 31, <?=$y?> - Nov 14, <?=$y?></option>
			<option value="11-15:15" <?=($cutoff=="11-15:15")? "selected" : null?>>Nov 15, <?=$y?> - Nov 29, <?=$y?></option>

			<option value="11-30:15" <?=($cutoff=="11-30:15")? "selected" : null?>>Nov 30, <?=$y?> - Dec 14</option>
			<option value="12-15:16" <?=($cutoff=="12-15:16")? "selected" : null?>>Dec 15, <?=$y?> - Dec 30, <?=$y?></option>
		</select>
	</div>
	<div class="pull-left">
		<strong style="float: left;margin-right: 10PX;margin-top: 5PX;">YEAR : <?=date("Y")?></strong> 
	</div>

		<div class="pull-left">
		<input type="submit" class="btn btn-warning btn-small btn-sm" value="Generate DTR Summary">
		</div>
		</form>
		<?php
			if($dtrsummary) {
				?>
				<div class="pull-left" style="margin-left:10px">
					<form action="<?=base_url()?>xadmin/payroll/submit/" id="submitPayroll" method="POST">
						<input type="hidden" name="cutoff" value="<?=$cutoff?>" />
						<input type="submit" name="for_payroll" id="for_payroll" class="btn btn-success btn-small btn-sm" value="Submit for Payroll">
					</form>
				</div>
				<?php
			}

		?>

</div>
<div id="xrole">
	<?php
		if($dtrsummary){
	?>
<table class="table table-hover table-striped table-custom display" style="font: 12px 'Arial';" id="dtr">
		<thead>
			<tr>
			   <th style="width:60px">EID</th>
			   <th>Employee Name</th>
			   <!-- <th style="width: 325px;text-align:center">Position</th> -->
			   
			   <th style="width:155px">Total lates</th>
			   <th style="width:155px">Total Overtime</th>
			  <th class="acl" style="width:90px;text-align:center">Total Hours</th>
			</tr>
        </thead>
		<tbody>
			
		<?php

		
			foreach ($dtrsummary as $get) {
				$a = "<tr><td class='text-align' style='width:155px;text-align:right'>".$get->eid."</td>";
				$a .= "<td class='text-align' style='width:155px;text-align:right'>".$get->lastname.", ".$get->firstname."</td>";
				$a .= "<td class='text-align' style='width:155px;text-align:right'>".convertToHoursMins($get->totallates)."</td>";
				$a .= "<td class='text-align' style='width:155px;text-align:right'>".convertToHoursMins($get->overtime)."</td>";
				$a .= "<td class='text-align' style='width:155px;text-align:right'>".number_format(($get->over_all_hrs/60),2)."</td></tr>";
			echo $a;
			}


		

		?>
			
		
			
		</tbody>
	</table>
	<?php
		}

	?>

		<?php

		if($payroll){
			?>
			<table class="table table-hover table-striped table-custom display" style="font: 12px 'Arial';" id="dtr">
		<thead>
			<tr>
			   <th style="width:60px">EID</th>
			   <th>Employee</th>
			   <!-- <th style="width: 325px;text-align:center">Position</th> -->
			   
			   <th style="width:30px;text-align:center">Basic</th>
			   <th style="width:155px">Lates</th>
			   <th style="width:155px">SSS</th>
			   <th style="width:155px">Pagibig</th>
			   <th style="width:155px">Philhealth</th>
			   <th style="width:155px">Overtime</th>
			   <th style="width:155px">Tax</th>
			   <th style="width:155px">Total Deduction</th>
			  <th class="acl" style="width:30px;text-align:center">Net Pay</th>
			</tr>
        </thead>
		<tbody>
			
		<?php

		if($payroll){
			foreach ($payroll as $get) {
				$a = "<tr><td class='text-align' style='width:155px;text-align:right'>".$get['eid']."</td>";
				$a .= "<td class='text-align' style='width:155px;text-align:right'>".$get['fullname']."</td>";
				$a .= "<td class='text-align' style='width:155px;text-align:right'>".$get['basicPay']."</td>";
				$a .= "<td class='text-align' style='width:155px;text-align:right'>".number_format($get['lates'],2)."</td>";
				$a .= "<td class='text-align' style='width:155px;text-align:right'>".number_format($get['sss'],2)."</td>";
				$a .= "<td class='text-align' style='width:155px;text-align:right'>".number_format($get['pagibig'],2)."</td>";
				$a .= "<td class='text-align' style='width:155px;text-align:right'>".number_format($get['philHealth'],2)."</td>";
				$a .= "<td class='text-align' style='width:155px;text-align:right'>".number_format($get['overtime'],2)."</td>";
				$a .= "<td class='text-align' style='width:155px;text-align:right'>".number_format($get['tax'],2)."</td>";
				$a .= "<td class='text-align' style='width:155px;text-align:right'>".number_format($get['totalDeduction'],2)."</td>";
				$deduction = "";

				$deduction .="<div class='clearfix'><div class='pull-left'>Lates :</div><div class='pull-right'> ".number_format($get['lates'],2)."</div></div>";
				$deduction .="<div class='clearfix'><div class='pull-left'>SSS :</div><div class='pull-right'> ".number_format($get['sss'],2)."</div></div>";
				$deduction .="<div class='clearfix'><div class='pull-left'>PagIbig :</div><div class='pull-right'> ".number_format($get['pagibig'],2)."</div></div>";
				$deduction .="<div class='clearfix'><div class='pull-left'>PhilHealth :</div><div class='pull-right'> ".number_format($get['philHealth'],2)."</div></div>";
				$deduction .="<div class='clearfix'><div class='pull-left'>Tax:</div><div class='pull-right'> ".number_format($get['tax'],2)."</div></div>";

				//$a .= "<td class='text-align' style='width:155px;text-align:left'>".$deduction."</td>";
				$a .= "<td class='text-align' style='width:155px;text-align:right'>".number_format($get['NetPay'],2)."</strong></td></tr>";
			echo $a;
			}


		}

		?>
			
		
			
		</tbody>
	</table>
			<?php
		}
		?>
	</div>
</fieldset>
<div id="upload" class="modal fade" tabindex="-1" data-focus-on="input:first"  data-keyboard="false" style="display: none;">
      <div class="modal-dialog" style="width:520px;margin-top: 15%;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="close" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Upload file</h4>
      </div>
      <div class="modal-body">
        <p style="color:#555">Upload Daily Time Record in Excel format. No format yet? <a href="<?=base_url()?>documents/DTR-format.xls" style="text-decoration:none"><span class="label label-success">Download Format</span></a></p>
			  
			  <form action="<?=base_url()?>xadmin/daily-attendance/upload" method="post" enctype="multipart/form-data" id="uploadFile">
					
					<input type="file" name="myfile" id="myfile">
					<p style="color:#555">Valid Format: .xls,.xlsx</p>
					<div id="status"></div>
					<div class="progress">
						<div class="bar"></div >
					</div>
					
					<input type="submit" value="Upload File" class="btn btn-success btn-sm" id="submit" style="display:none" />
				</form>
				
      </div>
    </div>
</div>

 
    
    
<script type="text/javascript" charset="utf-8" src="<?=base_url('assets/ver3.0/js/')?>jquery.dataTables.js" ></script>
<script type="text/javascript" charset="utf-8" src="<?=base_url('assets/ver3.0/js/')?>dataTables.bootstrap.js" ></script>
<script type="text/javascript" charset="utf-8" src="<?=base_url('assets/media/js/')?>ZeroClipboard.js" ></script>
<script type="text/javascript" charset="utf-8" src="<?=base_url('assets/media/js/')?>TableTools.js" ></script>
<script type="text/javascript" charset="utf-8" src="<?=base_url('assets/ver3.0/js/')?>jquery.PrintArea.js" ></script>
<script type="text/javascript" charset="utf-8" src="<?=base_url('assets/ver3.0/js/')?>jquery.form.js" ></script>
<style type="text/css">
.row{margin-left: 0px;margin-right: 0px}
.bar{background: #008DE6;
height: 20px;
width: 0%;
-webkit-transition: width .3s;
-moz-transition: width .3s;
transition: width .3s;}

.percent {
position: absolute;
display: inline-block;
color: #333;
}
.progress{display:none}
</style>
<script type="text/javascript">
var editor;
var forexport;
$(document).ready(function(){
		$.extend( true, $.fn.DataTable.TableTools.classes, {
				"container": "btn-group",
				"buttons": {
					"normal": "btn btn-default btn-sm",
					"disabled": "btn disabled"
				},
				"collection": {
					"container": "DTTT_dropdown dropdown-menu",
					"buttons": {
						"normal": "",
						"disabled": "disabled"
					}
				}
			} );

		/* Datatable decleration
		-----------------------------*/
		var oTable =  $('#dtr').dataTable({
		 	 "sDom": "<'row pull-left'T><'row pull-right forprint'f><'clr'>t<'row pull-right forprint'p><'clr'>",
		
		        "oTableTools": {
		            "sSwfPath": "<?=base_url()?>/assets/media/swf/copy_csv_xls_pdf.swf",
		           	"sRowSelect": "multi",
		             "aButtons": [
                                /*{
                                        "sExtends": "pdf",
                                        "sButtonText": "<span class='glyphicon glyphicon-export'></span> Export to PDF",
                                        "bSelectedOnly": true,
                                        "sFileName": "Daily Time Record.pdf",
                                        "mColumns": [0, 1,2,3,4]
                                },
                                {
                                        "sExtends": "xls",
                                        "sButtonText": "<span class='glyphicon glyphicon-export'></span> Export to Excel",
                                        "sFileName": "Daily Time Record.xls",	
                                        "bSelectedOnly": true,
                                      	"mColumns": [0, 1,2,3,4]

                                }*/
                             ]
		        }

		});
		/* end of datatable
		----------------------------------*/

var bar = $('.bar');
var percent = $('.percent');
var status = $('#status');
   
		$('#uploadFile').ajaxForm({
			dataType:  'json',
			beforeSend: function() {
				//status.fadeOut();
				bar.width('0%');
				percent.html('0%');
			},
			uploadProgress: function(event, position, total, percentComplete) {
				var percentVal = percentComplete + '%';
				bar.width(percentVal)
				percent.html(percentVal);
				$('#sumbit').attr('disable','true');
				$('.progress').css('display','block');
			},
			
			complete: function(xhr) {
				if(xhr.responseText==1){
					$('#sumbit').attr('disable','true');
					status.html("File was successfully uploaded!").fadeIn().css({'color':'green'});
				}else if(xhr.responseText==2){
					status.html("Upload Fail: Unknown error occurred!").fadeIn().css({'color':'red'});
				}else if(xhr.responseText==3){
					status.html("Upload Fail: Unsupported file format or It is too large to upload!!").fadeIn().css({'color':'red'});
				}else if(xhr.responseText==4){
					status.html("Upload Fail: File not uploaded!").fadeIn().css({'color':'red'});
				}else if(xhr.responseText==5){
					status.html("Bad request!").fadeIn().css({'color':'red'});
				}else if(xhr.responseText==7){
					status.html("File is Empty").fadeIn().css({'color':'red'});
				}else if(xhr.responseText > 7){
					result ="<p>Daily Time Record was successfully imported.</p><button type='button' data-dismiss='modal' class='btn btn-default btn-sm'>Ok</button>";
					$('.modal-body').html(result).css({'color':'green','text-align':'center'});
					$('#upload').on('hidden',function(){
						location.reload(true);
					});
				}
				//status.html(data.responseJSON.status).fadeIn();
			}
		}); 
	
	//setTimeout(function(){ jQuery(".alert-success").fadeOut(); }, 3000);
	
	
	$('#myfile').change(function(){
		$('#submit').css({'display':'block'});

	});

	$('#for_payroll').click(function(e){
		e.preventDefault();
		var con = confirm("Are your sure do want to submit?");
			if (con==false) {
				return false;
			}else{
				$('#submitPayroll').submit();

			}

	});
 });
 
 function select_file(){
	document.getElementById('image').click();
			return false;
}
</script>
