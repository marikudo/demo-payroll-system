<?php

class time_record extends crackerjack{
	
	public function __construct(){
		parent::__construct();
			
	}
	public function index(){
		echo "error";
	}

	public function daily($id){
		if (isset($_REQUEST['time']) && isset($_REQUEST['date'])) {
			$sms = true;
						$this->load->libraries(array("calDate"));
			$employee_id = $id[0];
			$emp = $this->crud->read("SELECT * FROM _temployee WHERE employee_id=:id",array(':id'=>$employee_id),'assoc');
			$date = date("Y-m-d",strtotime($_REQUEST['date']));
			$drop = array("AM","PM");
			$aTime = str_replace($drop,"", $_REQUEST['time']);
			$time = date("Y-m-d H:i:s",strtotime($date." ".$aTime));
			$checkDtr = $this->crud->read("SELECT * FROM _tdailytimerecord WHERE _date=:dt AND employee_id=:id",array(":dt"=>$date,':id'=>$employee_id),'assoc');
			$result['_date'] = $date;
			$result['eid'] = $emp['eid'];
			$result['employee_id'] = $emp['employee_id'];
			//print_r($result);
			$message = '';
			if ($checkDtr) {
				//print_r($checkDtr);
				//echo strtotime($checkDtr['timeout_morning']);
				if ($checkDtr['timein_morning']!="") {
					$nulltime0 = $checkDtr['timein_morning'];
					$nulltime1 = $checkDtr['timeout_morning'];
					$nulltime2 = $checkDtr['timein_afternoon'];
					$nulltime3 = $checkDtr['timeout_afternoon'];
					if ($nulltime1 == "0000-00-00 00:00:00") {
					
						$result['timeout_morning'] = $time;
						$this->crud->update("_tdailytimerecord",$result,array("dailytimerecord_id"=>$checkDtr['dailytimerecord_id']));
						echo $emp['firstname'];
					}

					if ($nulltime1!="0000-00-00 00:00:00" && $nulltime2=="0000-00-00 00:00:00") {
						$result['timein_afternoon'] = $time;
						$this->crud->update("_tdailytimerecord",$result,array("dailytimerecord_id"=>$checkDtr['dailytimerecord_id']));
						echo $emp['firstname'];
					}

					if ($nulltime1!="0000-00-00 00:00:00" && $nulltime2!="0000-00-00 00:00:00" && $nulltime3=="0000-00-00 00:00:00") {
						$result['timeout_afternoon'] = $time;
						//print_r($checkDtr);
						
						$start_date = new DateTime($nulltime0);
						$since_start = $start_date->diff(new DateTime($nulltime1));

						$start_datex = new DateTime($nulltime2);
						$since_startx = $start_datex->diff(new DateTime($time));
		
						$totalMinutes = ($since_start->i + $since_startx->i);
						$hours = intval($totalMinutes/60);
						$minutes = $totalMinutes - ($hours * 60);
						$totalHrs = ($since_start->h + $since_startx->h + $hours);
						$result['hrs'] = $totalHrs;
						$result['mins'] = $minutes;
						$this->crud->update("_tdailytimerecord",$result,array("dailytimerecord_id"=>$checkDtr['dailytimerecord_id']));
						echo $emp['firstname'];
					}
				}
			}else{
				$result['timein_morning'] = $time;

				$this->crud->create("_tdailytimerecord",$result);
				echo $emp['firstname'];
			}
				$aMiddle = $emp['middlename'];
				$message .= ucfirst($emp['firstname'])." ".ucfirst($aMiddle[0]).". ".ucfirst($emp['lastname'])."\n";
				$message .= "Date :".$date."\n";
				$message .= "Logged Time :".$_REQUEST['time']."\n";

				if ($sms==true) {
					
					 $strResult = "n/a";
					  $strRecipient = "[ToAddress]";
					  $strPincode = "";
			  
					  $objGsm = new COM("AxSms.Gsm", NULL, CP_UTF8 );
					  $objGsm->LogFile = sys_get_temp_dir()."Gsm.log"; 
					    $obj;
					    $strMessageReference;
					    $objSmsMessage   = new COM("AxSms.Message",    NULL, CP_UTF8 );
					    $objSmsConstants = new COM("AxSms.Constants" , NULL, CP_UTF8 );
					    


			    // $strName = $_POST["ddlDevices"];
			    // $strPincode = $_POST["txtPincode"];
			    // $strRecipient = $_POST["txtToAddress"];
			    // $iDeviceSpeed = $_POST["ddlDeviceSpeed"];

			    $strName = "";
			    $strPincode = "";
			    $strRecipient = "+639".$emp['mobile_number'];
			    $iDeviceSpeed = "0";
			    
			    $strDevice = $objGsm->FindFirstDevice();            
			            while ($objGsm->LastError == 0)
			            {
			              $strName = $strDevice;
			              $strDevice = $objGsm->FindNextDevice();
			            }
			    $objGsm->Clear();
			    $objGsm->LogFile = $objGsm->GetErrorDescription($objGsm->LastError);
			    $objGsm->Open($strName, $strPincode, $iDeviceSpeed);
			    
			    if ($objGsm->LastError != 0)
			    {
			      $strResult = $objGsm->LastError . ": " . $objGsm->GetErrorDescription($objGsm->LastError);
			      $objGsm->Close();
			    }
			    else
			    {
			      //Message Settings
			      $objSmsMessage->Clear();
			      $objSmsMessage->ToAddress = $strRecipient;
			      $objSmsMessage->Body = $message;
			     
			      // if (isset( $_POST["cbxUnicode"] ))
			      // {
			        $objSmsMessage->DataCoding = $objSmsConstants->DATACODING_UNICODE;
			      //}
			      
			      //Send the message !
			        if ($message!="") {
			        	# code...
				      $obj = $objSmsMessage;
				      $objGsm->SendSms($obj, $objSmsConstants->MULTIPART_ACCEPT, 0);
				      $objSmsMessage = $obj;
				      
				      $strResult = $objGsm->LastError . ": " . $objGsm->GetErrorDescription($objGsm->LastError);
				      
				      $objGsm->Close();
			        }
			    }
				}
		}

/*		$employee_id = $id[0];

		$emp = $this->crud->read("SELECT * FROM _temployee WHERE employee_id=:id",array(':id'=>$employee_id),'assoc');
		$date = date("Y-m-d");
		$result['_date'] = $date;
		$result['timein_morning'] = date("Y-m-d H:i:s",strtotime($date));
		$result['timeout_morning'] = date("Y-m-d H:i:s",strtotime($date));
		$result['timein_afternoon'] = date("Y-m-d H:i:s",strtotime($date));
		$result['timeout_afternoon'] = date("Y-m-d H:i:s",strtotime($date));
		$result['eid'] = $emp['eid'];
		$result['employee_id'] = $emp['employee_id'];
		$a = $this->crud->create('_tdailytimerecord',$result);
			if ($a) {
			
			$a =  '<html>';
			$a .=  '<head>';
			$a .=  '</head>';
			$a .=  '<body>';
			$a .=  '<script type="text/javascript">';
			$a .=  '</script>';
			$a .=  '<h2>Welcome '.$emp['firstname'].'! Have a Nice Day!';
			$a .=  '</h2>';
			$a .=  '</body>';
			$a .=  '</html>';

			echo $a;

			}*/
	}


}	