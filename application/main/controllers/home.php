<?php

class home extends crackerjack{
		public function __construct(){
			parent::__construct();
				
		}

		public function index(){
			if ($redirect = $this->session->_get('redirect_to')) {
					redirect($redirect);
				}
			$this->load->render('index');
		}

		public function auth(){
			if ($redirect = $this->session->_get('redirect_to')) {
					redirect($redirect);
				}
			if (isAjax()) {
				if ($_POST) {
				$isValid = 0;
					$_postData = $this->form->_serialize($_POST);
					//print_r($_postData);
					if (empty($_postData['username']) || empty($_postData['password'])) {
							$isValid = 1; 
						}else{
							$result = $this->user_auth->validate($_postData['username'],$_postData['password']);
							
							if ($result) {
								$redirect = '';
									if (isset($result['xroles_id']) && $result['xroles_id']>=0) {
										$redirect = 'xadmin';
										$this->session->_set(array('xadminlogin'=>true,'user_id'=>$result['xusers_id'],'role_id'=>$result['xroles_id'],'redirect_to'=>'xadmin'));
								
									}else{
										$redirect = 'profile';
										$this->session->_set(array('employeeLogin'=>true,'employee_id'=>$result['employee_id'],'redirect_to'=>'profile'));
									}
								$isValid = $redirect;
							}
					}
				echo $isValid;
		
				}
			die();
			}
		}

		public function forgot(){
			if ($redirect = $this->session->_get('redirect_to')) {
					redirect($redirect);
				}

				$this->load->render('forgot');

		}

		public function process(){
			if(isAjax()){
				$_postData = $this->form->_serialize($_POST);

				if ($_postData['credential']!="") {
						if ($_postData['type']=="email") {
							$username = $_postData['credential'];
							$record = $this->user_auth->isSystemUser($username);
							if($record==null){
								$record = $this->user_auth->email_exists($username);
								$email = $record['email'];
								$strName = $record['firstname'];
								$password = $this->hash->decryptMe_($record['password']);

							}else{
								$email = $record['email'];
								$strName = $record['first_name'];
								$password = $this->hash->decryptMe_($record['password']);

							}

							if ($email) {
								$subject1 = "Forgot Password";

								$from = "cathcutemoh@gmail.com";
								$headers1 = "From: Forgot Password <".$from.">\r\n";
								$headers1 .= "Reply-To: ".$from. "\r\n";
								$headers1 .= "MIME-Version: 1.0\r\n";
								$headers1 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

								$msg1 = "<html><head><title></title></head>";
								$msg1 .= "<body>";
								$msg1 .= "Dear ".$strName.",<br />";
								$msg1 .= "Your password :".$password."<br />";
								$msg1 .= "Thanks";
								
								$msg1 .= "</body></html>";
								$send1 = mail($email, $subject1, $msg1, $headers1);
								echo "Ok";
							}else{
								echo "Email not found.";
							}
							
						} else {
							
						}
						
				} else {
					echo "Please Enter your ".ucfirst($_postData['type']);
				}
				
			}
		}

		public function testing($a = false){
			 	$strResult = "n/a";
			  $strRecipient = "[ToAddress]";
			  $strPincode = "";
			  
			  $objGsm = new COM("AxSms.Gsm", NULL, CP_UTF8 );
			  $objGsm->LogFile = sys_get_temp_dir()."Gsm.log"; 
			  //Windows default: 'C:\Windows\Temp\Gsm.log'
			  
			  //Form submitted
			 /* if (isset( $_POST["btnSendMessage"] ))
			  {*/
			    $obj;
			    $strMessageReference;
			    $objSmsMessage   = new COM("AxSms.Message",    NULL, CP_UTF8 );
			    $objSmsConstants = new COM("AxSms.Constants" , NULL, CP_UTF8 );
			    


			    // $strName = $_POST["ddlDevices"];
			    // $strPincode = $_POST["txtPincode"];
			    // $strRecipient = $_POST["txtToAddress"];
			    // $iDeviceSpeed = $_POST["ddlDeviceSpeed"];

			    $strName = "";
			    $strPincode = "";
			    $strRecipient = "+639299611613";
			    $iDeviceSpeed = "0";
			    
			    $strDevice = $objGsm->FindFirstDevice();            
			            while ($objGsm->LastError == 0)
			            {
			              $strName = $strDevice;
			              $strDevice = $objGsm->FindNextDevice();
			            }
			    $objGsm->Clear();
			    $objGsm->LogFile = $objGsm->GetErrorDescription($objGsm->LastError);
			    $objGsm->Open($strName, $strPincode, $iDeviceSpeed);
			    
			    if ($objGsm->LastError != 0)
			    {
			      $strResult = $objGsm->LastError . ": " . $objGsm->GetErrorDescription($objGsm->LastError);
			      $objGsm->Close();
			    }
			    else
			    {
			      //Message Settings
			      $objSmsMessage->Clear();
			      $objSmsMessage->ToAddress = $strRecipient;
			      $objSmsMessage->Body = "body";
			     
			      if (isset( $_POST["cbxUnicode"] ))
			      {
			        $objSmsMessage->DataCoding = $objSmsConstants->DATACODING_UNICODE;
			      }
			      
			      //Send the message !
			      $obj = $objSmsMessage;
			      $objGsm->SendSms($obj, $objSmsConstants->MULTIPART_ACCEPT, 0);
			      $objSmsMessage = $obj;
			      
			      $strResult = $objGsm->LastError . ": " . $objGsm->GetErrorDescription($objGsm->LastError);
			      
			      $objGsm->Close();
			    }

			    echo $strResult;
		}
		
}