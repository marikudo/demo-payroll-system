<?php
if(!defined('APPS')) exit ('No direct access allowed');

class user_auth extends crackerjack_model{
	public function __construct(){
		parent::__construct();
	}

	public function validate($username,$password){
		$return = array();
		$record = $this->isSystemUser($username);
		if($record==null){
				$record = $this->email_exists($username);
				//print_r($record);
			}

		if ($record) {
			$dbpasswrod = $this->hash->decryptMe_($record['password']);

			//echo $dbpasswrod."==".$password;
			if ($password==$dbpasswrod) {
				unset($record['password']);
				$return = $record;
			}
		}else{
			unset($return);
		}
		return $return;
	}

	public function email_exists($email){
		$return = false;
		$result = $this->crud->read("SELECT * FROM _temployee WHERE email=:username",array(':username'=>$email),'assoc');
			if ($result) {
				$return = $result;
			}
		return $return;
	}

	public function isSystemUser($email){
		$return = false;
		$result = $this->crud->read("SELECT * FROM _xusers WHERE email=:username AND xroles_id > 0",array(':username'=>$email),'assoc');
			if ($result) {
				$return = $result;
			}
		return $return;
	}

	public function forgot($email){
		$result = $this->email_exists($email);
			if ($result) {
				$return = $result;				
			}
		return $return;
	}



}