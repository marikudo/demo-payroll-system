<?php
if(!defined('APPS')) exit ('No direct access allowed');

class user_auth extends crackerjack_model{
	public function __construct(){
		parent::__construct();
	}

	public function validate($username,$password,$table){
		$return = false;
			switch ($table) {
				case '_temployee':
					$result = $this->email_exists($username);
					break;
				
				default:
					$result = $this->admin_exists($username);
					break;
			}
			if ($result) {
					$dbpasswrod = $this->hash->decryptMe_($result['password']);
					if ($password==$dbpasswrod) {
						unset($result['password']);
						$return = $result;
					}
			}
		return $return;
	}

	public function email_exists($email){
		$return = false;
		$result = $this->crud->read("SELECT * FROM _temployee WHERE email=:username",array(':username'=>$email),'assoc');
			if ($result) {
				$return = $result;
			}
		return $return;
	}

	public function forgot($email){
		$result = $this->email_exists($email);
			if ($result) {
				$return = $result;				
			}
		return $return;
	}



}