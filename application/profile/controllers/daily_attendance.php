<?php
class daily_attendance extends crackerjack{
	public $count = 0;
	protected $employee_id;
	public function __construct(){
		parent::__construct();
		$this->employee_id =$this->session->_get('employee_id');
		}
	public function index(){
		if(islogin()==false){
				redirect('home');
			}
		$data['attendance'] = $this->crud->read("SELECT * FROM _tdailytimerecord WHERE employee_id=:id ORDER BY dailytimerecord_id DESC",array(':id'=>$this->employee_id),'obj');
		$this->template->employeeTemplate('employee/daily_attendance',$data,$this->load);

	}
}