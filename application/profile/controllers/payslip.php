<?php
class payslip extends crackerjack{
	public $count = 0;
	protected $employee_id;
	public function __construct(){
		parent::__construct();
		$this->employee_id =$this->session->_get('employee_id');
		}
	public function index(){
		if(islogin()==false){
				redirect('home');
			}
			$this->load->libraries(array('compensation'));
			$dtrsummary =  $this->crud->read("SELECT *,tr.employee_id,tr.eid,sum(hrs) as totalHrs, sum(mins) as totalMins, sum(late_hrs) as totallateHrs, sum(late_mins) as totallateMins, sum(total_in_mins) as totalrhrs, sum(total_late_in_mins) as totallates,sum(overtime) as overtime, sum(over_all_hrs) as over_all_hrs FROM _tpayslips AS tp INNER JOIN _tdailytimerecord AS tr ON tr.payslip_id = tp.payslip_id INNER JOIN _temployee AS e ON tr.employee_id = e.employee_id WHERE tr.employee_id = :id",array(":id"=>$this->employee_id),'obj');
				foreach ($dtrsummary as $get) {
						
						$monthlyRate = $get->rate;
						$semimonthlyRate = ($get->rate / 2);

						$dailyRate  = ($monthlyRate * 12) / 365;
						$hourlyRate = $dailyRate / 8;
						$minuteRate = number_format( $hourlyRate / 60 , 2);
						$minuteRate2 = $hourlyRate / 60 ;
						//echo $get->totallates."=".$minuteRate."=".$hourlyRate."=".$dailyRate."<br />";
						 $basicPay = $semimonthlyRate;
						/*overtime here*/
						$overtime = $get->overtime * $minuteRate2;
						/*deduction*/
						
						$lates = ($get->totallates * $minuteRate);
						$sss = $this->compensation->getSSS($semimonthlyRate) / 2;
						$pagibig = $this->compensation->getPagIbig($semimonthlyRate) / 2;
						//$philHealth = $this->compensation->getPhilHealth(double($monthlyRate)) / 2;;
						$philHealth = $this->compensation->getPhilHealth($semimonthlyRate) /2;
						$def = ($late + $sss + $pagibig + $philHealth);
						$taxable = $basicPay - ($def);
						$tax = $this->compensation->getTax($taxable,'single');
						$totalDeduction = ($def + $tax);

						$netPay = ($basicPay - $totalDeduction);

						$payroll[$get->employee_id]['eid'] = $get->eid;
						$payroll[$get->employee_id]['fullname'] = $get->firstname." ".$get->lastname;
						$payroll[$get->employee_id]['NetPay'] = $netPay;
						$payroll[$get->employee_id]['basicPay'] = $basicPay;
						$payroll[$get->employee_id]['sss'] = $sss;
						$payroll[$get->employee_id]['pagibig'] = $pagibig;
						$payroll[$get->employee_id]['philHealth'] = $philHealth;
						$payroll[$get->employee_id]['tax'] = $tax;
						$payroll[$get->employee_id]['lates'] = $lates;
						$payroll[$get->employee_id]['overtime'] = $overtime;
						$payroll[$get->employee_id]['totalDeduction'] = $totalDeduction;
						$payroll[$get->employee_id]['dateStart'] = $get->date_start;
						$payroll[$get->employee_id]['dateEnd'] = $get->date_end;
						$payroll[$get->employee_id]['dateSumitted'] = $get->update_at;
						//$payroll[$get->employee_id]['dateSumitted'] = $get->update_at;

						$isUPdate++;
						//$dtrsummary = $this->crud->read("UPDATE _t",array(":StartDate"=>$StartDate,":EndDate"=>$EndDate),'obj');
				


					}


					 $data['payslip'] = $payroll;
			$this->template->employeeTemplate('employee/payslip',$data,$this->load);

	}
}