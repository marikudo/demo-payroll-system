<?php

class overtime extends crackerjack{
	public $count = 0;
	protected $employee_id;
	public function __construct(){
		parent::__construct();
		$this->employee_id =$this->session->_get('employee_id');
		}
	public function index(){
		if(islogin()==false){
				redirect('home');
			}
		$data['request'] = $this->crud->read("SELECT * FROM _temprequest WHERE employee_id=:id AND leavesettings_id = 4 ORDER BY emprequest DESC",array(':id'=>$this->employee_id),'obj');
		$this->template->employeeTemplate('employee/my_overtime',$data,$this->load);

	}

	public function new_record(){
		if ($_POST) {
			if (isAjax()) {
				if(isset($_POST['sendrequest'])){
					$result = $this->form->post('sendrequest');

					$result['date_from'] = $aDate = date("Y-m-d",strtotime($result['date_from']));
					$a = $this->crud->read("SELECT * FROM _temprequest WHERE date_from=:d",array(':d'=>$aDate),'assoc');
						if ($a) {
							echo false;
						}else{
							$result['date_to'] = date("Y-m-d",strtotime($result['date_to']));
							$result['created_at'] = date("Y-m-d");
							$result['leavesettings_id'] = 4;
							$result['employee_id'] = $this->session->_get('employee_id');
						echo $this->crud->create("_temprequest",$result);
						}

				}
			}
			die();
		}
		$data['action'] = 'add';
		$data['vacation_prior'] = 0;
		$this->template->employeeTemplate('employee/my_request_overtime',$data,$this->load);
	}
	
	

	public function view($params){
		$eID = $this->hash->decryptMe_($params[0]);
		$data['action'] = 'view';
		$data['result'] = $this->crud->read("SELECT * FROM _temprequest WHERE emprequest = :id",array(':id'=>$eID),'assoc');
		//$data['vacation_prior'] = $this->crud->read("SELECT * FROM _tleavesettings WHERE leavesettings_id = 1",array(),'assoc');
		$this->template->employeeTemplate('employee/my_request_overtime',$data,$this->load);
	}


		
}