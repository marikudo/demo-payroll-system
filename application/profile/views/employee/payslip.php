<div class="container-emp">
	<fieldset class="title-container">
		<legend>Payslip </legend>
    <?php
  if($payslip){
  ?>
<table class="table table-hover table-striped table-custom display" style="font: 12px 'Arial';" id="dtr">
    <thead>
      <tr>
         <th style="width:60px">Date Submited</th>
         <th style="width:60px">Date From</th>
         <th>Date To</th>
        <th style="width:30px;text-align:center">Basic</th>
         <th style="width:155px">Lates</th>
         <th style="width:155px">Overtime</th>
         <th style="width:155px">SSS</th>
         <th style="width:155px">Pagibig</th>
         <th style="width:155px">Philhealth</th>
         <th style="width:155px">Tax</th>
         <th style="width:155px">Total Deduction</th>
        <th class="acl" style="width:30px;text-align:center">Net Pay</th>
      </tr>
        </thead>
    <tbody>
      
    <?php

    
           //$data['payslip'] = $payroll;
      foreach ($payslip as $get) {
        $a = "<tr><td class='text-align' style='width:155px;text-align:right'>".date("Y-m-d",strtotime($get['dateSumitted']))."</td>";
        $a .= "<td class='text-align' style='width:155px;text-align:right'>".$get['dateStart']."</td>";
        $a .= "<td class='text-align' style='width:155px;text-align:right'>".$get['dateEnd']."</td>";
        $a .= "<td class='text-align' style='width:155px;text-align:right'>".$get['basicPay']."</td>";
        $a .= "<td class='text-align' style='width:155px;text-align:right'>".number_format($get['lates'],2)."</td>";
        $a .= "<td class='text-align' style='width:155px;text-align:right'>".number_format($get['overtime'],2)."</td>";
        $a .= "<td class='text-align' style='width:155px;text-align:right'>".number_format($get['sss'],2)."</td>";
        $a .= "<td class='text-align' style='width:155px;text-align:right'>".number_format($get['pagibig'],2)."</td>";
        $a .= "<td class='text-align' style='width:155px;text-align:right'>".number_format($get['philHealth'],2)."</td>";
        $a .= "<td class='text-align' style='width:155px;text-align:right'>".number_format($get['tax'],2)."</td>";
        $a .= "<td class='text-align' style='width:155px;text-align:right'>".number_format($get['totalDeduction'],2)."</td>";
        $deduction = "";

        $deduction .="<div class='clearfix'><div class='pull-left'>Lates :</div><div class='pull-right'> ".number_format($get['lates'],2)."</div></div>";
        $deduction .="<div class='clearfix'><div class='pull-left'>SSS :</div><div class='pull-right'> ".number_format($get['sss'],2)."</div></div>";
        $deduction .="<div class='clearfix'><div class='pull-left'>PagIbig :</div><div class='pull-right'> ".number_format($get['pagibig'],2)."</div></div>";
        $deduction .="<div class='clearfix'><div class='pull-left'>PhilHealth :</div><div class='pull-right'> ".number_format($get['philHealth'],2)."</div></div>";
        $deduction .="<div class='clearfix'><div class='pull-left'>Tax:</div><div class='pull-right'> ".number_format($get['tax'],2)."</div></div>";

        //$a .= "<td class='text-align' style='width:155px;text-align:left'>".$deduction."</td>";
        $a .= "<td class='text-align' style='width:155px;text-align:right'>".number_format($get['NetPay'],2)."</strong></td></tr>";
      echo $a;
      }

    

    ?>
      
    
      
    </tbody>
  </table>
  <?php
}
  ?>
		
	</fieldset>
</div>

<script type="text/javascript">
  $(function(){
      $(document).ready(function(){
          //$('#request').dateTable();
              /* Datatable decleration
    -----------------------------*/
   var oTable =  $('#request').dataTable({
          "sDom":"rtp<'clear'>",
            "sScrollX": "775px",
            "iDisplayLength": 50});
      });

  })
</script>
