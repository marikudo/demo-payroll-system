<div class="container-emp">
	<fieldset class="title-container">
		<legend>Vacation leave </legend>
	<!-- <div class="btn-group">
  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
    Request <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
   <li class="dropdown-header">Leave</li>
    <li><a href="<?=base_url()?>profile/my-request/vacation">Vacation </a></li>
    <li><a href="<?=base_url()?>profile/my-request/emergency">Emergency</a></li>
    <li><a href="<?=base_url()?>profile/my-request/sick">Sick</a></li>
    <li class="divider"></li>
    <li class="dropdown-header">Other</li>
    <li><a href="<?=base_url()?>profile/my-request/overtime">Overtime</a></li>
  </ul>
</div> -->
<a href="<?=base_url()?>profile/my-request/vacation" class="btn btn-success">Request</a>
<table class="table table-striped" id="request">
  <thead>
    <tr>
      <th>Date Request</th>
      <th>From</th>
      <th>To</th>
      <th>Reason</th>
      <th>Request</th>
      <th>Status</th>
      <th><center>Action</center></th>
    </tr>
  </thead>
  <tbody>
    <?php
      if ($request) {

          foreach ($request as $get) {

             $request = '';
            switch ($get->leavesettings_id) {
              case 1:
               $request = 'Vacation Leave';
              break;
               case 2:
               $request = 'Emergency Leave';
              break;
               case 3:
               $request = 'Sick Leave';
              break;
               case 4:
               $request = 'Overtime';
              break;
              
              default:
                # code...
                break;
            }

            ?>
            <tr>
              <td><?=date("Y-m-d",strtotime($get->created_at))?></td>
              <td><?=$get->date_from?></td>
              <td><?=$get->date_to?></td>
              <td><?=readmore($get->reason,15,"...")?></td>
              <td><?=$request?></td>
              <td><?=($get->request_status==1) ? '<span class="badge badge-success">Approved</span>' : '<span class="badge badge-danger">Pending</span>'?></td>
              <td><center><a href="<?=base_url()?>profile/my-request/view/<?=genKey($get->emprequest)?>">View</a></center></td>
            
            </tr>
            <?php
          }
      }
    ?>
  </tbody>
</table>
		
	</fieldset>
</div>

<script type="text/javascript">
  $(function(){
      $(document).ready(function(){
          //$('#request').dateTable();
              /* Datatable decleration
    -----------------------------*/
   var oTable =  $('#request').dataTable({
          "sDom":"rtp<'clear'>",
            "sScrollX": "775px",
            "iDisplayLength": 50});
      });

  })
</script>
