<div class="container-emp">
	<!-- <img src="<?=base_url()?>assets/employee/images/sitel.jpg" style="width: 150px;margin-right:10px" class="pull-left"/>   -->     
	<p style="margin:0">Welcome to</p>
	<h1 class="system">Payroll System</h1>
	<p style="margin:0">Make Payroll Fast & Easy. On payday, your payroll system will precisely calculate and automatically create paychecks.</p>
	<br />
	<?php
		//print_r($request_result);
		if ($request_result) {
			foreach ($request_result as $get) {
				$request = ($get->leavesettings_id==1) ? 'Vacation Leave' : 'Overtime';
				$link = ($get->leavesettings_id==1) ? 'my-request' : 'overtime';
				 $message = "You have <strong>(<a href='".base_url('profile/').$link."/?dashboard=true'>".$get->count."</a>)</strong> ".$request." Approved Request";
				?>
				<div class="alert alert-success" style="margin-bottom: 5px;">
					<p><?=$message?></p>	
				</div>
				<?php
			}
		}


		if ($request_result_denied) {
	//	print_r($request_result_denied);
			foreach ($request_result_denied as $get) {
				$request = ($get->leavesettings_id==1) ? 'Vacation Leave' : 'Overtime';
				$link = ($get->leavesettings_id==1) ? 'my-request' : 'overtime';
				//echo 1;
				 $message = "You have <strong>(<a href='".base_url('profile/').$link."/?dashboard=true'>".$get->count."</a>)</strong> ".$request." Denied Request";
				?>
				<div class="alert alert-success" style="margin-bottom: 5px;">
					<p><?=$message?></p>	
				</div>
				<?php
			}
		}
	?>
</div>
