<div class="container-emp">
	<fieldset class="title-container">
		<legend>Daily Attendance </legend>


<table class="table table-striped" id="request">
  <thead>
    <tr>
      <th>Date</th>
      <th>Time In</th>
      <th>Time Out</th>
      <th>Time In</th>
      <th>Time Out</th>
      <th>Lates</th>
      <th>Overtime</th>
      <th>Total Hours</th>
    </tr>
  </thead>
  <tbody>
   <?php
      if ($attendance) {
        foreach ($attendance as $get) {
        //  $morning = getTotalMinutes($get->timein_morning,$get->timeout_morning);
          ?>
          <tr>
            <td><?=$get->_date?></td>
            <td><?=date("H:i:s",strtotime($get->timein_morning))?></td>
            <td><?=date("H:i:s",strtotime($get->timeout_morning))?></td>
            <td><?=date("H:i:s",strtotime($get->timein_afternoon))?></td>
            <td><?=date("H:i:s",strtotime($get->timeout_afternoon))?></td>
            <td><?=$get->late_hrs.".".$get->late_mins; ?></td>
            <td><?=number_format(($get->overtime / 60),2)?></td>
            <td><?=$get->hrs.".".$get->mins;?></td>
          </tr>
          <?php
        }
      }
   ?>
  </tbody>
</table>
		
	</fieldset>
</div>

<script type="text/javascript">
  $(function(){
      $(document).ready(function(){
          //$('#request').dateTable();
              /* Datatable decleration
    -----------------------------*/
   var oTable =  $('#request').dataTable({
          "sDom":"rtp<'clear'>",
            "sScrollX": "775px",
            "iDisplayLength": 50});
      });

  })
</script>
