<?php
class compensation{

	public function __construct(){

	}

	public function getSSS($amount){
		$contribution = 0;
		
		if($amount >= 1000 && $amount <= 1249.99 ) {
			$contribution =	33.3;
		}elseif ($amount >= 1250.00 && $amount <= 1749.99 ) {
			$contribution =	50;
		}elseif ($amount >= 1750.00 && $amount <= 2249.99 ) {
			$contribution =	66.7;
		}elseif ($amount >= 2250.00 && $amount <= 2749.99 ) {
			$contribution =	83.3;
		}elseif ($amount >= 2750.00 && $amount <= 3249.99 ) {
			$contribution =	100;
		}elseif ($amount >= 3250.00 && $amount <= 3749.99 ) {
			$contribution =	166.7;
		}elseif ($amount >= 3750.00 && $amount <= 4249.99 ) {
			$contribution =	133.3;
		}elseif ($amount >= 4250.00 && $amount <= 4749.99 ) {
			$contribution =	150;
		}elseif ($amount >= 4750.00 && $amount <=  5249.99 ) {
			$contribution =	166.7;
		}elseif ($amount >= 5250.00 && $amount <= 5749.99 ) {
			$contribution =	183.3;
		}elseif ($amount >= 5750.00 && $amount <= 6249.99 ) {
			$contribution =	200;
		}elseif ($amount >= 6250.00 && $amount <= 6749.99 ) {
			$contribution =	216.7;
		}elseif ($amount >= 6750.00 && $amount <= 7249.99 ) {
			$contribution =	233.3;
		}elseif ($amount >= 7250.00 && $amount <= 7749.99 ) {
			$contribution =	250;
		}elseif ($amount >= 7750.00 && $amount <= 8249.99 ) {
			$contribution =	266.7;
		}elseif ($amount >= 8250.00 && $amount <= 8749.99 ) {
			$contribution =	283.3;
		}elseif ($amount >= 8750.00 && $amount <= 9249.99 ) {
			$contribution =	300;
		}elseif ($amount >= 9250.00 && $amount <= 9749.99 ) {
			$contribution =	316.7;
		}elseif ($amount >= 9750.00 && $amount <= 10249.99 ) {
			$contribution =	333.3;
		}elseif ($amount >= 10750.00  && $amount <= 11249.99 ) {
			$contribution =	366.7;
		}elseif ($amount >= 11250.00  && $amount <= 11749.99 ) {
			$contribution =	383.3;
		}elseif ($amount >= 11750.00  && $amount <= 12249.99 ) {
			$contribution =	400;
		}elseif ($amount >= 12250.00  && $amount <= 12749.99 ) {
			$contribution =	416.7;
		}elseif ($amount >= 12750.00  && $amount <= 13249.99 ) {
			$contribution =	433.3;
		}elseif ($amount >= 13250.00  && $amount <= 13749.99 ) {
			$contribution =	450;
		}elseif ($amount >= 14250.00  && $amount <= 14749.99 ) {
			$contribution =	483.3;
		}elseif ($amount >= 14750.00) {
			$contribution = 500;
		}
		return $contribution;
	}	

	public function getPhilHealth($amount){
		//echo $amount;
		$contribution = 0;
		if($amount <=4999.99){
			$contribution =	50;
		}elseif ($amount >= 5000.00 && $amount <= 5999.99) {
			$contribution =	62.5;
		}elseif ($amount >= 6000.00 && $amount <= 6999.99) {
			$contribution =	75;
		}elseif ($amount >= 7000 && $amount <= 7999.99) {
			$contribution =	87.5;
		}elseif ($amount >= 8000.00 && $amount <= 8999.99) {
			$contribution =	100;
		}elseif ($amount >= 9000.00 && $amount <= 9999.99) {
			$contribution =	112.5;
		}elseif ($amount >= 10000.00 && $amount <= 10999.99) {
			$contribution =	125;
		}elseif ($amount >= 11000.00 && $amount <= 11999.99) {
			$contribution =	137.5;
		}elseif ($amount >= 12000.00 && $amount <= 12999.99) {
			$contribution =	150;
		}elseif ($amount >= 13000.00 && $amount <= 13999.99) {
			$contribution =	162.5;
		}elseif ($amount >= 14000.00 && $amount <= 14999.99) {
			$contribution =	175;
		}elseif ($amount >= 15000.00 && $amount <= 15999.99) {
			$contribution =	187.5;
		}elseif ($amount >= 16000.00 && $amount <= 16999.99) {
			$contribution =	200;
		}elseif ($amount >= 17000.00 && $amount <= 17999.99) {
			$contribution =	212.5;
		}elseif ($amount >= 18000.00 && $amount <= 18999.99) {
			$contribution =	225;
		}elseif ($amount >= 19000.00 && $amount <= 19999.99) {
			$contribution =	237.5;
		}elseif ($amount >= 20000.00 && $amount <= 20999.99) {
			$contribution =	250;
		}elseif ($amount >= 21000.00 && $amount <= 21999.99) {
			$contribution =	262.5;
		}elseif ($amount >= 22000.00 && $amount <= 22999.99) {
			$contribution =	275;
		}elseif ($amount >= 23000.00 && $amount <= 23999.99) {
			$contribution =	287.5;
		}elseif ($amount >= 24000.00 && $amount <= 24999.99) {
			$contribution =	300;
		}elseif ($amount >= 25000.00 && $amount <= 25999.99) {
			$contribution =	312.5;
		}elseif ($amount >= 26000.00 && $amount <= 26999.99) {
			$contribution =	325;
		}elseif ($amount >= 27000.00 && $amount <= 27999.99) {
			$contribution =	337.5;
		}elseif ($amount >= 28000.00 && $amount <= 28999.99) {
			$contribution =	350;
		}elseif ($amount >= 29000.00 && $amount <= 29999.99) {
			$contribution =	362.5;
		}elseif ($amount >= 30000.00) {
			$contribution = 375;
		}


		return $contribution;
	}

	public function getPagIbig($amount){
		$contribution = 0;
			if ($amount <= 1500.00) {
				$contribution = $amount * (1/100);
			} else {
				$contribution = $amount * (1/100);
			}
			

		return $contribution;
	}

	public function getTax($amount,$isMaried){
		$contribution = 0;
		/*
			ref : http://www.rappler.com/business/53216-income-tax-calculator
		*/
			switch (strtolower($isMaried)) {
				case 'single' || 'married':
						
						$sMEstatus = array(0,.5,.10,.15,.20,.25,.30,.32);
						$sME = array(1,2083,2500,3333,5000,7917,12500,22917);
						$exemption = array(0,0,20.83,100.17,354.17,937.50,2083.33,5208.33);
						$tax = 0;
						$exmption = 0;
						$status = 0;
						foreach ($sME as $key => $value) {
								if ($value < $amount ) {
									$tax = $value;
									$exmption = $exemption[$key];
									$status = $sMEstatus[$key];
								}
						}
						$contribution = $exmption + (($amount -$tax) * $status);
						
					break;
				
				default:
					# code...
					break;
			}


/*		if($amount >= 2 && $amount <= 2083){
			$contribution =	0;
		}elseif ($amount >= 2084	&& $amount <= 2500)
		{
			$contribution =	20.83;
		}elseif ($amount >= 2501	&& $amount <= 3333) 
		{
			$contribution =	104.71;
		}elseif ($amount >= 3334	&& $amount <= 5000) 
		{
			$contribution =	354.17;
		}elseif ($amount >= 5001	&& $amount <= 7917) 
		{
			$contribution =	937.5;
		}elseif ($amount >= 7918	&& $amount <= 12500) 
		{
			$contribution =	2083.33;
			}elseif ($amount >= 12501 && $amount <= 22917) {
			$contribution =	5208.33;
		}
*/


		return $contribution;
	}



}