<?php
function getTotalMinutes($date_from,$date_to){
	$to_time = strtotime($date_to);
	$from_time = strtotime($date_from);
	$diff = $to_time - $from_time;
	$mins = round(abs($to_time - $from_time)/60,0, PHP_ROUND_HALF_DOWN);
	return $mins;
}

?>