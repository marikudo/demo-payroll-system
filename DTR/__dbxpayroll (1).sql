-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2015 at 04:06 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `__dbxpayroll`
--

-- --------------------------------------------------------

--
-- Table structure for table `_holidays`
--

CREATE TABLE IF NOT EXISTS `_holidays` (
`holiday_id` int(11) NOT NULL,
  `holiday` varchar(254) NOT NULL,
  `_date` varchar(10) NOT NULL,
  `holiday_type` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_holidays`
--

INSERT INTO `_holidays` (`holiday_id`, `holiday`, `_date`, `holiday_type`) VALUES
(1, 'New Year''s Day', '01-01', 1),
(2, 'Chinese Lunar New Year''s Day', '02-19', 2),
(3, 'Maundy Thursday', '04-02', 1),
(4, 'Good Friday', '04-03', 1),
(5, 'The Day of Valor', '04-09', 1),
(6, 'Labor Day', '05-01', 1),
(7, 'Independence Day', '06-12', 1),
(8, 'Ninoy Aquino Day', '08-21', 2),
(9, 'National Heroes Day', '08-30', 1),
(10, 'National Heroes Day holiday', '08-31', 1),
(11, 'Bonifacio Day', '11-30', 1),
(12, 'Christmas Eve', '12-24', 2),
(13, 'Christmas Day', '12-25', 1),
(14, 'Rizal Day', '12-30', 1),
(15, 'New Year''s Eve', '12-31', 2);

-- --------------------------------------------------------

--
-- Table structure for table `_holiday_type`
--

CREATE TABLE IF NOT EXISTS `_holiday_type` (
`holiday_type_id` int(11) NOT NULL,
  `holidy_type` varchar(254) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `percentage` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_holiday_type`
--

INSERT INTO `_holiday_type` (`holiday_type_id`, `holidy_type`, `status`, `percentage`) VALUES
(1, 'Regular Holiday', 1, 200),
(2, 'Special Non-working Holiday', 1, 130);

-- --------------------------------------------------------

--
-- Table structure for table `_tdailytimerecord`
--

CREATE TABLE IF NOT EXISTS `_tdailytimerecord` (
`dailytimerecord_id` int(11) NOT NULL,
  `eid` varchar(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `_date` date NOT NULL,
  `timein_morning` datetime NOT NULL,
  `timeout_morning` datetime NOT NULL,
  `timein_afternoon` datetime NOT NULL,
  `timeout_afternoon` datetime NOT NULL,
  `hrs` int(2) NOT NULL,
  `mins` int(11) NOT NULL,
  `total_in_mins` int(11) NOT NULL,
  `overhrs` int(11) NOT NULL,
  `over_mins` int(11) NOT NULL,
  `erhrs` int(11) NOT NULL DEFAULT '8',
  `late_hrs` int(11) NOT NULL,
  `late_mins` int(11) NOT NULL,
  `total_late_in_mins` int(11) NOT NULL,
  `payroll_status` int(11) NOT NULL DEFAULT '0',
  `payslip_id` int(11) NOT NULL,
  `isholiday` int(11) NOT NULL,
  `holidayrate` decimal(10,2) NOT NULL,
  `overtime` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_tdepartment`
--

CREATE TABLE IF NOT EXISTS `_tdepartment` (
`department_id` int(11) NOT NULL,
  `department` varchar(254) NOT NULL,
  `date_created` datetime NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_tdepartment`
--

INSERT INTO `_tdepartment` (`department_id`, `department`, `date_created`, `last_update`, `status`) VALUES
(28, 'Desktop Publishing', '2014-09-26 20:53:27', '2014-09-26 18:53:27', 1),
(27, 'Editorial', '2014-09-26 20:52:56', '2014-09-26 18:52:56', 1),
(26, 'Marketing', '2014-09-26 20:52:42', '2014-09-26 18:52:42', 1),
(25, 'Circulation', '2014-09-26 20:52:30', '2014-09-26 18:52:30', 1),
(24, 'Accounting', '2014-09-26 20:52:16', '2014-09-26 18:52:16', 1),
(23, 'Production', '2014-09-26 20:52:03', '2014-09-26 18:52:03', 1),
(29, 'Graphic Design', '2014-09-26 20:53:55', '2014-09-26 18:53:55', 1),
(30, 'Human Resource(HR)', '2014-09-26 20:57:15', '2014-09-26 18:57:15', 1),
(31, 'Layout', '2015-02-01 06:57:55', '2015-02-01 05:57:55', 1);

-- --------------------------------------------------------

--
-- Table structure for table `_temployee`
--

CREATE TABLE IF NOT EXISTS `_temployee` (
`employee_id` int(11) NOT NULL,
  `eid` varchar(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `firstname` varchar(254) NOT NULL,
  `middlename` varchar(254) NOT NULL,
  `lastname` varchar(254) NOT NULL,
  `rate` int(11) NOT NULL,
  `civil_status` varchar(254) NOT NULL,
  `avatar` varchar(254) NOT NULL,
  `telephone` varchar(254) NOT NULL,
  `mobile_number` varchar(254) NOT NULL,
  `hire_date` date NOT NULL,
  `address1` varchar(254) NOT NULL,
  `address2` varchar(254) NOT NULL,
  `address3` varchar(254) NOT NULL,
  `_sss` varchar(254) NOT NULL,
  `_philhealth` varchar(254) NOT NULL,
  `_pagibig` varchar(254) NOT NULL,
  `_tin` varchar(254) NOT NULL,
  `email` varchar(254) NOT NULL,
  `password` varchar(254) NOT NULL,
  `status` int(11) NOT NULL,
  `min1` text NOT NULL,
  `min2` text NOT NULL,
  `min3` text NOT NULL,
  `min4` text NOT NULL,
  `date_created` datetime NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_temployee`
--

INSERT INTO `_temployee` (`employee_id`, `eid`, `position_id`, `firstname`, `middlename`, `lastname`, `rate`, `civil_status`, `avatar`, `telephone`, `mobile_number`, `hire_date`, `address1`, `address2`, `address3`, `_sss`, `_philhealth`, `_pagibig`, `_tin`, `email`, `password`, `status`, `min1`, `min2`, `min3`, `min4`, `date_created`, `last_update`) VALUES
(38, '1013', 24, 'Ghiear', 'C', 'Obana', 0, 'Single', 'avatar_54e0113ba08b1.jpg', '', '209183018', '0000-00-00', '67', 'hsakjhd', 'hajkhd', '131-23-1231', '23-12313232323-2', '2321-3123-2321', '23-2323232', 'ghie@gmail.com', 'OrITMYvvF1dmGAo::HRh6+ymR0mL/tmThX7rtckQMpW/d3xsY/BA=', 1, '0PQp6wgjWa8WykBrRovw3ROpQ2eklXJT3DfD7c44N4gH0O0yXiNEuX0HU8getZHivvwFe4eTmycRJOyROGapcKC1ttu9kqfstQzfMe4brBc/MF/fnLLdyVmD02aCyvOCRfo0eEOjhUqyur2nCdgs2F2G1KGw/5fgGsKwQ98PPja4b7QX7am73LjDtJtc0iw*lVY0FK1eN7yhg/B8OuPxxhstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'y1/zJOfMSHekgSYj2wolndtbA4gyr5C2rtJBOLocUvaa0iHxMYcbBxmhWXkujjpY*HctLyu*5BID/u79sgDBdaNZNyxy6G/7G5qN8fhBl3VuqYkgNqhoR3hFm*pMCiTVE9MF2u8yLumKRR3LGpYN0monZK8X4hKLbAXCFJrBKLe8AAkMLk2qKvN9WDDoE/B4Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'EFSUwfzXXG4*jCYR54hmottbA4gyr5C2rtJBOLocUvYeKfnng*324feNQtKGjHL0dADesXzQ4EguTCYZLVIDY58gH3BDOWnokrpfM4maFGGApdLE*Lb3jQMO3i4U2lmgsFBPogqAiC6aYG4I7WmzktbRZ6AsFvcFAitAaU5Qg4bMH8795VqIZ1UAs3fJqAKWGy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'EFSUwfzXXG4*jCYR54hmottbA4gyr5C2rtJBOLocUvZKo*8O9n2/N59B22X3djUHdADesXzQ4EguTCYZLVIDYwAYuuLSTD/o9DfdCEJ1VSfqVA/okqARAjmmzYL4J9OO5/Xc3mLWUvlCiLsP2yYHVNbRZ6AsFvcFAitAaU5Qg4bMH8795VqIZ1UAs3fJqAKWGy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2014-10-01 17:21:03', '2015-02-15 03:23:42'),
(42, '1017', 24, 'varbie', 'A', 'Berecio', 15000, 'Married', 'no_avatar.jpg', '', '989878678', '2014-10-21', 'gfhgfghs', 'ghfsghfg', 'fshgf', '978-76-7564', '77-98989786756-4', '6768-7989-7867', '80-9887656', 'varbie@gmail.com', '4zFEXhwKIblZ2HF::NtJFzn9KahLaZ9S9L3PcfFzfprFghGxdugY=', 1, 'CFWJgyD5FCkzjXeEa9IZHbGXNg17Dg6H0ysMgV52HTCobiWItgTQ4j9710ObCVhCJC4QY/Fe5tdqLGNpLOJRQzlXGVQVliI6cBsiZkdw*WbU0HpdaYS/PJSP2Hn6ZPVU/bRqAr5qbG/f2BkiBODbR6IiLVj3K/cvkVtBPVC59kyNIZgDrDENujsI0i7*4y7y/70h/WHOi73V34m0S78DpUrw7sGaCBGNbdpOHA/0dKmo4KIuvSxv4xj*3Zn6REz4wbZtfMdG7LkLslAp5wczexstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'ryKmfHg5eQND2RES7pjLzpg5Wj3AMgLzyv589W1IkGiozQxl2NuKseUF0U7aM4O*eFrVCLghDwxbVRLvJ4iTpXvD5aDQYjBz0pn*Co/ZQ88xWxiup6xLfd*kC*hRRwnsD3EsnlRGEd2KiaGCIByRwZ8oFQHKFYaNMQFKHp*bcIbCRyFrFuTBN3AOgxKftlQReVLngn5HjYRtB02owta*6Hi*Npn766egUda/Q7pwxvdSTTOs1eG9CqOSdTOda7/cWek4YeEwhVJZZh1zzUq20RstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'yrfLc3F7YeAD84aUb7EkptRr9YoR*stsWE4XPy4FWCu1VyWJ8HAnBz/nUoAg4GsV8JHi3b36esJfNeQjnUNnrMilU3dhFQJYwEKej5RMOr0v4F9u/fEbnkNUR3WBsn9U6g7IA1gu8mhZeXCut1VkUaJ9oVQPLsEEn11Vuw0zpLrfqtxtYxTBCvB1JR8QjCioQN6aKBoz4CeuuLadZNS4sx7IdWFfy69qmXI1CoWL9cEbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'Pr/vnvEiGD5wJ8Lvz5ItyqsbLwn3Y87Jfh0SrVde9eBsyHArCVH9YrohYbC6sK31ObVHLgpxP00cB3VCCwjaree0V3M4TK/Gqgz3Lc3r/7feYkPMMSciv*sIJfTbD8z2Pbt84LymtARg6kgt/GcGEtgtw4heV5fBNTErxZc3FzthJQvNYopYvTzkKz2wpUPqxy6I922t02jZRiNenlXuRCK0cp5Mo*C2K7FUTScPGmXoJVebaf5xHLLNUL8u4/fRGy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2014-10-21 05:36:51', '2015-01-30 17:27:39'),
(49, '1122', 22, 'Brenda Lyn', 'Matu', 'Baniqued', 20000, 'Single', 'no_avatar.jpg', '', '466348776', '0000-00-00', '651', 'gen', 'luna', '665-57-6765', '43-54354676777-6', '5446-5446-5654', '45-6575563', 'brenda.baniqued@gmail.com', 'JohQrmlNMx2ypy2::gYyBHxpCNb30wIS1jF1xXnhUgT+mE0lsa9w=', 1, 'VENBX9ClkiZy258FJSyLDWiW7ntcTYZTczuPpqMhDZCB2dLlEqL6e5hsWN/SkFx*/KkJDHckB1HA161xsHweVNH*aHjPSRfhG1tUmgGcQX3pS90NbDNFpMmuNZQ/utK7HqcMbVMQS1FN35Vf6BwqB5yg2vry5CMJrHhAiqRFIZCbyfOwswq1YZs/TsV0ncpz74woABuuZmbodSp4qbDUKd*71SYbORXTGMunrP3H9NQbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'lD6CZwEwVcZ5JNOJ7NmYJi0rg7OxeDD1vm3ve0q86uYDAY*3YzEVoF/cwLK*wXG7cfOLsH5IX*8qEQS5Kto1JPwxGFP0kOMVNi70t0pdulSZ*6xcwWHr9/9lPd0slGWAcmIdGYom8WJddft7gjdqdIk/JBaQWTqoz9vvhecvRJgbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'Kb2rOsUkELZX6SBuuyzmH82WvZ33xkbMkQBL11x1SznOWcBOh40D5Vtty/Trp8wZBknjhAs6t*UbDzII6gg*Ufwbhynrj8sqRSyT7D*/1/Wb/szCalgxWL7Jy*TqDWv1scWm8yqhG3JdcE7eaO7Cym4ql67Ksd9*cW4fCIYU84LEHh0UTkjZjnwdNRNimpa307I*/praV/*1Vg0pR*XT9RstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'EfJyvvZyob/691xcTvjSDADwNHPmtspMjvxUTzPOC7jQlsD2Ys4WNWOnBfsXLeVnRbmIJzJ4U/ZWaekXEa2yr/ebnNqE7S7Eql/EVibaxOZutz5NZNrVRn2Uj57iquTxLGAlwKzJL1K0vn07OgancD/wdg5k3fBVfJtxer*wrXek*4Jz7WEx*uFeqkZ81aVlGy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2015-01-29 07:21:16', '2015-02-01 17:12:57'),
(50, '1123', 24, 'kkkll', 'kjhk', 'jkh', 0, 'Single', 'no_avatar.jpg', '', '466348776', '0000-00-00', '55', 'fgfgf', 'fgfgfg', '342-34-2435', '43-55446575465-4', '2345-4657-5775', '34-3546567', 'brendalyn@gmail.com', 'PmwpbMx44ixBgLH::+3OOIFodlwTx/5cItPUllEDgUdKdZo5gfL8=', 1, 'SYixlzc3Ur2SnDwzjfHczEoCehm5jsVhofR64/tp7nicci7O4SfUTZT0viJOkn/YjUzKghw6uPpC66Fy9HCiUMrbjOlVRFjuDdvv4s0Ym9EQxyCC/mH*zDKME5zRFrgCGMQsYJje2GGDDz6Bupcp5VvQjWpmfdyy6DHEvD0S0aXi1fZhGdqXAM*iFLRA*7DXfVTnDaLZO*OqJYqE/4hqOxstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '*SRTqB8YhhQNW2DFudnb4W5Qd0ZBtpEl69GeoOtXLiUxCvOopa/TDGcO4rqVGip0u0VE7VImUlkTdUHQ9obWkyggm9xOHy40EnBBz1emDtagBBJ45pYbiHOr9srMr7*T8UQhgdT4SSQwcPyTUUGUGXEzeVo3IzDEdgw0c2AtrzD*AHpQbTHp85jsqMvFNTIFi6jBQUYd12TH31L3ktnluRstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '9PlvLS5P2zX7VoRtSgywCihOqdyW1z*qpE/obLardLyTs**hUGcXNh10R78sIhhAXgfAM*RXy9KAnVRVj6NdVXTPJ3etWNCm4aAPHucYhVYbzYdz4QrQzSCzvoaH/Lg*gISbGzRAGXiZpjv2lU9yanXyGxKTzm39cLN8qSzf0uBKWneSplmHI37mgFTSMJguMENCKUVZk*vdl/Msn6LHbxstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'QuBOxeVU/wzqG35PuUrCUwva5NJqeMnlsbtewaYZwkw22JkwbIGJU32eExCAvRzVLddJ7Du0mQ34CPCvbyKMn1fOmajQ89YEs*4rMgRuYyhqyuFvCAmZOT3X8irpn1g38B9FaTqySaj88YqHr09hNOfxK4m3XarXmhJENdYYESIbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2015-01-31 08:01:06', '2015-02-15 03:07:09'),
(51, '1124', 25, 'Catherine', 'Chavez', 'Tolentino', 20000, 'Single', 'avatar_54e0116e5b6d6.jpg', '', '124119319', '0000-00-00', '657', 'Donya Maria St.', 'Sampaloc, Manila', '978-76-7565', '78-97990896775-6', '7566-7687-9080', '78-7667453', 'cathcutemoh@gmail.com', 'WG63rRMgDE9FFi9::7unDvL7DnxdaN5NEQLvjvK3EH/9Y9vWkKwooky0=', 1, '3CPeD7z80eao0E5dB90Oy4LD0wxmyDzsomo4BFOetnaPVtl/yIR1w*pFHEJNijvNkHjwEnxMnMIq7DLWtt2OQr74JwdkbmZfbCygI4to/3r2sowD4ucw66bfNAhr*X5hAUlo*k51kycQ29FfwwjyfhstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'AxONUnmGb4nvHbMlpB14a4LD0wxmyDzsomo4BFOetnazBoYqT7XC*jygnz0HMSexxHoyEcYEfe*ebfnOMz8rK1XaXXNaGN9XjKmqYADp9IOjek7u2ljlr2Cou9Dg8tvp4/7JlOy4xwV/Z5C2BU0ABhstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'kRnww14QTvFLnzlRD2Qy4waCUUZ0kTJkb5Bc4gq0JhQXjYrXPPgKKnfOdl2g5NzfnoIbLyic4I0T5h8HjpZRF1aHL3GW4QUxkgkaT3xWn*M64KUBRyFSsiOMuVIWy4hfRGxEzNB7VoAAsx/s6JfSShstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'kRnww14QTvFLnzlRD2Qy4wusBTRt9sqaV41cSkCj6ofcfcmKS7644c2fkAOdwSjmYXOvzXpqvESUDxZ3CaESjp/ZJrkI0udSD8vQ9kyQZOFv6gpb/DTys*vfC9HwIBZhc0SjbC3J4lyd8Tnn1BT7uRstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2015-02-01 09:09:46', '2015-02-15 03:54:44'),
(52, '1125', 17, 'lorem', 'lorem', 'lorem', 25000, 'Single', 'avatar_54ce032528be4.jpg', '', '929961161', '2015-02-01', '32', '23', '23', '890-89-0890', '80-98908908908-9', '8908-9089-0890', '89-0890890', 'lorem@gmail.com', '1ptIgHmz6mQYEh5::WYOie8YvhjkKVuvhWm34lnwKdLqMbqumhBI=', 1, '', '', '', '', '2015-02-01 11:43:45', '2015-02-01 10:43:45'),
(53, '1126', 25, 'Leah', 'coc', 'clashofclan', 20000, 'Married', 'no_avatar.jpg', '', '466348776', '2015-02-01', '6544', 'tacloban', 'taclo', '655-65-5653', '65-64645454545-4', '9855-4686-5454', '08-7667456', 'leahcoc@gmail.com', '3zJ51CjSHGdmsBd::USf2tDsaoo/Q6ugAGCHXjDdsCdB3hV4eIFhO', 1, 'xXxLrGGuz/aXjNxxd5IzgT6CnCkO1AdUMDssNAQvKXGryWGHIkcBoI8bDzlEP2*IebdnH3lW/4johaguuhjyptILtccwGtEIbNRLB6Db*fC1Fq5J5bzN4FLzg4ZWN1jzZ5HVUV0jqH1JZFQopCsppPixDdkLpEBg9YVnZmN1TjN21J65TpxQsMm/VlUe0*S43tjDVZr7R*YNjRFdhlR16/8FO1atqcVBV9Lw5FLdYgobLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'L8qlq6C0On7Bdf5UlQz/7v*wNN0eRADfo14H/dd2obW6iBdDIy9oAwl*rmiQ0KPanrvJflCfRfy5iHAcDQJC01v*JkP9a6clZkofMfAazwTgCSYIV7Lh*HWxXE50if3fAXOhXuyFh9jV3IHjtNyt1cGJlgCyAv3tIAVa8F9ly1ywSZo2pXD2*3nVyUQO7inEhBoBt0PIvcs0iczLAS0MaxstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'El3v1IwEKplgGfsoa9n8GTgQPu3WQqDRygKy/i3mkKzCPquX7soi7cqlzQq3F9iXRsjPg2NPYyl66GRGJWNUSbqXSYExWljA5aO17QkrGRx*gHAug/SIm9vKPznmlDxs01WqhGIoAvzTpLjJpWji8pnSLzDNAafiGr0iP*C9cDs3YukgE/3x9dUmmkQsw8eq*1Pok/P79xau6qfJiRFw4hstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'UWmvswXbtmG8DMIC88ZAwb2ENUUAr5alE0eEvrv2O*fZscrG1mJhIb2bxSjDYjkUn/h*JkpOCieLrsn95PO5B1B1c*Pg4N0Z1zxtH/qSJ010LacGAIlOI1TJQRAFo7m11P/RGNjidRa7o0Bh9GUVAhCo/Zxqik4YrZEnR7qYDIuY6el/Jw2d2pAqHl33BjL3RlJPquchsPbVz063KwiPcHUh0SLWPdHExsW8hCzuQDMbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2015-02-01 18:10:17', '2015-02-01 17:11:16'),
(54, '1127', 25, 'Tine', 'Marcilla', 'Bobiles', 15000, 'Single', 'no_avatar.jpg', '', '475006952', '2015-02-03', 'Block 7 Lot 12 Asamba Cmpd.', 'Bgy. Baesa', 'Quezon City', '123-45-6789', '12-34567834567-5', '1234-5678-9087', '23-4567898', 'nmbobiles@gmail.com', '0E3IwyR9bi9mFip::LwHntJ7kRUqJM7maACr9SLdOOHTrm2yXOZll', 1, 'fhkiJdYiaPm8RauJLmeU0ZPnbV2TP8a85FMIx2kVi*9vVtJllCPtvkB2*y4rp6RFnA*1uErwUKzAPIKLHU8ydbXF5734xi/6hABgSNUqi2Rw7UgZun4nWeZyPpbDp846P2puzyB/6XCiOjxhhDASpHcgvTpcSzKVs/i2LW0VyLKBjMQvSAsFnge03hy4faBF1MQ4AqRDJ2sQxeL2hBGIJdUHc25Iuua*dD3C/bNO6ERRmfjdHQniLIvMLeh6PGZiCuK2oq1inAsCt0T/qL4lWCYWn695m2ou3dX*t4GkEXIbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'Q4IsEbGA2SYiQMOiETfNsd/OLdBPrKsdkKJ6IhYYUZo2YrOZHqPHaxfL0mo4QjwwfCjIwBxsS/JHr0U96aAVVBNkv4IzCWBjGkacOmTQdaGahAqTD4aAuLzsRQdWaqu7st9dFo85xHe9Msc8EbhNwcrUtcE7vG7gGRGhexFmWrujsN66khEa92K4KjxKAAZUWIDa*TEW*r1PLE1vpyQ3nSuIYUlc2XxI*20VgR8PdGAzLsPVfLC6j7t1uJtgwA2S7b64W9Zt/lADCEgb2cjpYk1Y6acutdNpRkbY/NnyqXobLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'Nbpimsfw1oMAPtEzCtm2bdTiMdCqcU2Y4MTyUhqhA7QLuf8Mylwo2FpB4rY/1TF5PTxQbE7WtXvLBLInXTA2QQN2Ajd6vzWw99v9l0QMDZUseZmgscOCIUQKTPPKJA23y*e9cmmb/WIIh77b58LbfqL34/UvzJP/g52OVJO3rBDCJg/tDAMcCu1Yuv1CdKp1/X65JLnwrx7lpdL5Jd7ZVVZ3ODiwkrd9NEqi26h*WoESXT*ca8SAVICpl/z9W0cDJlrA/1CcydQkKKOf2IssRRstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'fKWv/T*qiyrS0cZAUpsfJjLdh/x*wVuPg4G3esAPYkKGBEV1utcFeW5DfDt*pG4BA5mmpeH41EWjJRYOlsBuQIIUkVYoPZIw4Sh1wqiDozkdRplLJz2Ao2GGhs*OYLdV6RaePvaGT9P26a/eiKVs4qlSh50lViup5OpKZHWKAdlFW9qkfWhsszWuk3Mib6sAxgUsLpC7UH1qO7riZzRTfBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2015-02-03 01:12:28', '2015-02-03 00:13:29'),
(55, '1128', 20, 'Lhea', 'Obog', 'Villaluna', 15000, 'Single', 'no_avatar.jpg', '', '15673591', '2015-02-04', 'vbn', '', '', '289-47-2561', '23-45676543543-5', '2345-6754-3456', '34-5676543', 'lheavillaluna@gmail.com', 'dLe5rD9BF28Vlej::nwUuItW+sGOtXTKzWp7RwtG5XmGf4drE5VNP3DFP', 1, 'lhiU9jQrMszIYc6iP5**Gh/PtYOcoJjiCNdmXNElgNzo5EdrMSHBTVnkBJXJtetWPMLuc4JbFpdM/kIdi*elCNi965vlktzLXHolORqrBJkakLU5dYFxUb5X2JXStSvQchjtFeKE0Ccnw2YhasAq9izxXIDiRlCsr*qK1g4fkMc6KoCiFiPayAKbkrGfxSzAWz7/Sb3hLvjsZXlPfIV*t794P7p4pmQXE7OvqLfhYUMbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'XQr7QAzQLMY4lP*GkQF8tWx7XyyVDDt4imD7jeVw95t0DdsHP6uuW2NOwDu5Q4lUxgFrJG4SXdWmYmssVnSGjwiJxSgy79F5yADykI8OpKMei0tkKvrNtla1WWF75P*iDvWHA*SAEfPttZEgBQZIy0kYqACfafjWMigDBXRTYFIIR7ISJ9W1obsXoXzcis4qac5W4ajqLWhloC/wpWyhOxstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'Ka5U8n7038oUSIe*qX/nsxtspyFgfVsin7FcMY2NmEgkHHN3rc2NAQvM4J6zLRmG27PTACE2MxW0NKZ9FAsBapPjXc0Fbx4uHG7QT22WjXkrYVNojOR*559iD9DAs3YhsST3IEGYOajIHFB*eYDd1lat8kn/oVa7Tx0GNhIgKQGRbpBGFKht8A582x*XMDvR2mdff85W8CI2/vVfccCDGz3Cti/4Xpuw94KVbN*cXtFsw85vEL3MGw3q7TK*WssQGy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '4fbXE6SndAX2MNw57lBALgTEdP5mkI7WEm75o50y2XKXcUJt2X2viu0rl6H70g1SgnFMGHUNQREMpaUh80DM0ovGeJNp/xmiIaBpPNzrIRxPfWVQExSN4vTs79UGIlYduxo9*JgVXEJKdMKGqtxelirK2uDi9k8GaYnifgDirfdn2gBISNLzqS33WzzW5*i9apNYcyz3rwtFutyxkwhmV05Rv1T03ywvHgAKi5pxufPzPm3j89WGHuqhIjbmVg7gwrFFLY6I3wqJW96QlNMAwxstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2015-02-04 05:33:25', '2015-02-04 04:34:22'),
(56, '1129', 22, 'benjamin jr', 'lomallo', 'alba', 20000, 'Single', 'no_avatar.jpg', '', '057552353', '2015-02-04', '1067', 'tabang guiguinto', 'bulacan', '987-65-4321', '54-32167890541-2', '8765-4323-4567', '33-2324353', 'benjalbajr@gmail.com', 'RcWivHPmHaNmUqK::+qlEsRve68CR62jz+75FgCDxCnCI+zcjqYVIxg==', 1, 'ilau*buw3u9gfNwvHlu5L6G*M0PBys4x4Sl0gZXI8RIZrRUJZ2fnhxqig3G/wCPebccVA*Wsxx42RvKid7Cs0mY/XhYUDgMEbNW5nzi0flvkWJtafhfZiYHgqM06pJctq*jmdNIFgf2PzujtT5pEJRM8ReNqdiimHobgXqSu/bKHrvT/9rWj0jLwV3RMtsgxG0rwIIO9m6E2*Glrt6irsWgnhBCVkwOg25hvJWx*Y4rItm/t5y0zZ0cQWM7KTnLwGy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '1E2*65fCaeuT4GgRrZ8cVyT*Em7yr6whwFd9tjI01IxBHB1XiorqkRg7mFq0bYzOScvCrqXj5YqUA0XHSgFEzIbL3AIEA5h6N*Ov68HSXu7P2lNs1oXrnZmulbxIjl49glEVF1Q9XbiheNd6pbYoxcON42SyYCyrP4szv1a4PWsD5*rNN0c0EnVshvgOYolCStDaBDnO5Q99*6fSWHqiVpSf63iR2*F7D7q4A6LclKX5X*PgpI7OFR2ypMmPBtuM46YcMLXOgYKFkuCPdSvrWRstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'Q9asqkflW9I0V77F2zb6F2e0X95nb/n/BzzohpO8SsZ5m7dOy0osCQ7sdLjL7NKOrl900Eiq8cteQFvpcTYZDghyIKJvrN0YPlAKHBppOn0xIqvymoUv4BpPkeReYKq0orlYIU0Scob7AuC2LjlfhVCpDkUd2hJghsep6OHdvDfPgcNsGX7q0LgN8Y27zMbrFlSNzTuxnzed9dWkrsrx3FK*jeAjq5xT8vSwzRX6dW68nYnWHCIv8lJh9yLyQWzvK*Sv1RhL8Ejae9BWsxErUgc/8L**JXdCTUe6XFdqLTkbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'PsocWgBsM/rBb38AcbcAlPlodluL5AAnUCwvJNTEm0HD76b695rI7dx4e7PTHWEtL4xri4rDs6vtad*I0o78Ao3Qf49fQ5FtBjpDMF3PQMvnZge*ixGUFYFrz9VkTecwL5IWiyebV68L1piya5e8gs9hX0tchV80iHl0okmegQpDLhYRuCc/uR7OKwhVXtTouqZBljOdJSzQgxS/JpyE4cq0OLDkakOLd8R/WxCN5UsfzSd/99ugxrg*UJnp5iHfyMDr3/8r3sUcqPdrgC37G/HxBaCy4q0F/5GlxdftWfobLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2015-02-04 05:47:46', '2015-02-04 04:48:50'),
(57, '1130', 20, 'Arlene', 'Belbis', 'Campos', 20000, 'Single', 'no_avatar.jpg', '', '477675119', '2015-02-04', 'BLK 82 LOT 9 ROAD 42 KAVAKAVA ST., ROBINSON HOMES EAST', 'BRGY. SAN JOSE', 'ANTIPOLO CITY', '234-56-7809', '34-56789098765-4', '3456-7890-9876', '34-5678909', 'arlenecampos05@gmail.com@gmail.com', 'SEKdDN1Fjik6sOG::L+FWmzrdjHqORDht2nBUbUjQxxrjVARxAZHfuWo=', 1, '3IsdiCNlGJFJa9dTfAnKqh2gF0m2JSmQx0t0svDn4tlAeOrWlVgB80JVdCjP5qaF5qc8z9jYpieJ6qkRMfS55yzcLG9lPBD4lW7A/DW6YPP9BemhL89jbXpT71KoGaVKuTgqOo/mz8rXDen2UD19XGH1ebtYjB*3U2RqkYIHwnNwhUeplYBtLwEgk/0Zw1TbWSKHWZSuHU4Zl7o2H8ybViWU7yLazcCOYFRRLKkicZE3qmh4SrfHlr4RGHIDtCatGy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'dj19uxrNnDaJQVsUzD12vkRnnsaH5HO9M3y191msKIMcUCOoZm1E6A65XZAl/*fKl9Xng1W4EhKAN51E6U4E5hwQYNmyMp4GsCw8qVU/wIGGk/2uQOCqWK360xdxEcq1kUBO4kGMTWUch9j8HpLccctBPlvsNjXFWqIs7gZRaW4YpgqP9wF8Ef079rR1sPbI1VLOb0P5ALig5mgMWLhzBwJufmRFW0tUE5Xyv38t*JW607xdthC/UjzcWUTuZ2sXCDkPk4S/93wUAI13CWAKHRstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'RPhMNjBe3r/l2Wrm7*aDukvSiGr1PTbog1EzT05*YRCBh4/6hgyEsBw7o*qrNsPsuWnO85DI7g6U8gJLv1uUKPB9Cxf1ZquVeyrun*ZCaW4c8H/58YdANAL5Dpr*xChp9sXphzVoljzlPNXgoRD78nALxC8DHeDpYj0/kVXZMN*1aTdk7ibbQlqjKnpG1drjbSShDlZi1ZmmWxI57fcAdSkMn0YyVyRMUWaoKcCfFcR1MsU7K*UADyYLM*2/aCQ5fXQ7zjRGIjxbsq7BPShtWRstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'Zajyk**utb0q6mR5YN36WWesP*EZIXMXkmKoOR14coplx9JQxtPaxamgAUPcX2cLjyB9ixZbul7D/A/Ouavlk3OEnsAnBlDyGzMABw9Xt2egj96TqQDeaaim*mCrSHZeew1TRjxpElS9i7oVBGYRy51tWyUgG9ZZvMLFQ7jjGzBL6UfbIuzIwhCQb7fvdBh7s1m/ZJ8cOCo124RiqRiMRKiIjBw1zPrb9GGFKu9HEICPB9R2fVo2DG*oEjN1m9D7w1kcD/Z1QvzDoRl5H2Sh2BstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2015-02-04 06:02:30', '2015-02-04 05:03:14'),
(58, '1131', 20, 'Anna Cecilia', 'Festin', 'Benedicto', 15000, 'Single', 'no_avatar.jpg', '', '920923247', '2015-02-04', '37 Conchy Street', 'Barangay Masagana', 'Quezon City', '899-12-2347', '09-87654321123-4', '7865-2080-8411', '90-8676345', 'liafestin@yahoo.com', 'BLTdYFUcmCGF5Vk::BHewI1UvsoNdxJld6zsBhiCusdv/zLic5zsdY16n', 1, 'mgMzr2gTDXFyer6745xKU8cTY6PHDjOwxY5l/7FDN8DcbIdXC8eFCDVSw1twJOu*GiqmFaVMmnezDFEefQ77sl0vWpHYSuBkba6/zk1Y5NsX9TWOlgsAEEdmUs6spZFCe3ceMMpF8fY7v6zJdghcNriJFzCI5jOWlq7kSI/HAHvZPe/4Q6XNl1LrW4nfbAQsGy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'vIUgh3Tzbx/ptE2hV5VSgVJ3k79ab*2udIGIUp9R*DhiDFiglm3S54l6UyM26bRpbPdR87dMh8zpp1YPUZLe*k56eFJNHUcZqaHAWM/Yzmdg1e64s7WfXoLj6tlLEy*WRyIFY0hYfOO98n/fmPwNXWxiQctEDJqej/PKmgPuTfJncc/SSfZdzCIQb2dgxIlmpM8DV1Fvq1PwB*FEP*K3kxstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '5TMxzXY0cM13wNhmpeelN9YLDQf2AO5SuNak*RgmD3MnaJgq/AIyEhEKCaeL9Yos3It4MhJ/qPvoqM/AGb8v0YfKAORTsS7plh5*SOXbqBl3QCJGnJqgAK1KGvJblAxOcH3xHuNBhVlBKq92UtZ0PyWgeh9j1uOtjf/5Fxgb7l1gw9P2eekdqjpgy/O43MITvztcusv6fFfwm69n4hNoHhstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'JNbWixcGmdNKlrJTaW6zxlopWiRfp*zL/mmW3s8KVAhekH4am/J7gNiVayvp5DeJLfLP6EgCu98CY5eNRbieAMOcMQn83fWavEzSAr5*lZNPKXespMFfloRrm2SCoRVT2Ok1NB9fEWLQCdPANlZ5AEe1E9BmNq4ZrXHSb1E4wgNIHdGjLDeqM/H9LIr1mKAy0YrsvXC*Ro1cfH/yfhZJcTAMIXD7cKJR9BaWHNziiXwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2015-02-04 06:11:28', '2015-02-04 05:12:21'),
(59, '1132', 22, 'ricardo', 'caberto', 'tansioco', 15000, 'Married', 'no_avatar.jpg', '', '274112563', '2015-02-04', '2903 lola binday st. villa alfonso', 'bambang ', 'pasig city', '234-56-7123', '23-45678653458-6', '6980-0090-0212', '56-6789098', 'ric.tancute@yahoo.com', 'dgp8LbNNxRMXkXZ::GiF5s1F6Lxf1hwWVk/1L35uahS11woF/anvFs74IUNk=', 1, 'gSkly64pIXF4ksWIsb9Xrl2xSGfouSjy/VuScMIFvPnZawXDYUq8kxV4sUhE1QvN5viy4SMxXzcb8oLPa7YG4QSmPoGDcjoGJMl2*mLt1gaKQrgQfAFtra8yp*zT4sb308WwggdUCFvgbp47FJ89s7cm/E67FPeCAHyOE97sKr8xIFGPSKy*vmBMEahWmxXTGy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'mgMzr2gTDXFyer6745xKU2uPEsu30cQq1NVNTyFoHrBnwyl9LQ3SGyHRo7*Ob2cdb8tTFoDzBLSufU3dxwVyZLnb7mJLPDT50*Ekh29Gk3bq1JWd*8cAM0X5i9C0PfFhQs7Kl9ITpsojg/EewAJXc3GmqGkuAxtZytqFah/3ufPQy0Z3VZazbfSuUV7zHUFZGy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'mgMzr2gTDXFyer6745xKU3o2ylCsrgD9qkBbPtPZIPHJp7lRCGB0w2ETEz4KY5a4UFwoRyVGZI0dFnH4qe79F5*OyuDEISRYHXza1wwv6I9hpUOWp3dF7f7YuoaM2FinudmXnmqpuh8mbDR3aZx6Luoy3/o*MYQhTVXaxXTUTniiOp9DD8RxWuPNAkY2o7p9Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'mgMzr2gTDXFyer6745xKU6FoPWZoS7/bWHEXcmNhJunbJIp1fmFUJrjV90cwoXdc/aJ6/G*Mhdvm2CfCgJb7urMxN5214j5Rghy2MNpuSMUBBL3KsZKYfDWKF5U/gOFp4t49He4f1RPJVQm/tanP8JJNNi1l0Pi9qw9NREZbcoihNHB8UFj3ju1IzdB/yqV5Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2015-02-04 06:18:05', '2015-02-04 05:19:09'),
(60, '1133', 22, 'gilbert', 'baldorado', 'lazaro', 30000, 'Married', 'no_avatar.jpg', '', '983262696', '2015-02-04', 'blk5lot3 ayala alabang gilid', '', '', '098-76-5432', '12-34567890987-8', '1234-5678-9009', '22-2222222', 'dyebi@yahoo.com', 'Mg8pp873PMUSlFY::qppdgDYYLM17bNjGwwrX3G3HeXvbdhib9+saZg==', 1, 'nZevLkf4bSA9euFOohupTCJgdeAYEJZLpasxHQC*s6EKc/StgBRRgznLHo*3l*6gRo3BPWxz4*vCY9ROemv*6/2e0Z7VQeaOn9A/V7arnfUbPKMXSpXESbVGZkacZ6bCRsQoLYH91HiCzfFvwWRqGj0QxQblkBVASTOPJU27TklYifqv2nLmR*sTFdcyLbePGPo6Q4seg/rpziHwCK65VxstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'NAzKme0CX4nzXGlYwwAzy6wU5s9mlOSRjA2z/Y1gTgibiVj6w4Q89jBwma/5RO9QdTvlzZpqDJKVe2xweVyT3baWfJE56g5CssflFOnqIrEFJDsxk8d9407THxue8Gt3YgNWFed3LPkc0e/Lj4mTaH02YwGQvEBFc9G7UA8i4JdLDTJxEuzgjM*3mPmcabFWGy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'QisIggUlYSo1*s0nXLyRvlGq*47Fz2lmA29j2lUIDuTuh7NnrIGfbw9BubAbmCxoGxaOFkUYPyCbVCjuDl77M/xFVXeZHjawfwK4dHxhGfO*pAsBs7Y3NS*Nx8hJ2cXYDISzoLF1CaVuIqSjQQSTFZBCJaLwDj1xyu1YTH4*KhNlcM/jOOYEu4GhwVVmb1fMJdKLWdKKtkHdiH31Ugm5LhstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'KTEmBXhuuVzGDXfs3ZUkOjChNJg44tgL3kFZZAumVhRaCE1*MK7u2iqwgMmSe3CmlnYMFLdCr7KZyhmHE1Bd5HlRjYwVkd/EoRTMuqjiORi1yqkUjRo8wxKCaqyO8yB/jnF8rMzKdiiUSPpCov*j*axBgbZaMwQ7dSmToTGk3QvjnFXIc4400Ss2npoaChtl7r0ggfUgayvR4JVcy3zAXRstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2015-02-04 06:23:04', '2015-02-04 05:23:44'),
(61, '1134', 20, 'Zhusmita May', 'Pabilan', 'Manangan', 15000, 'Single', 'no_avatar.jpg', '', '264848937', '2015-02-04', '1723 E. Jorge St.,', '898/100', 'Manila', '000-00-0000', '00-00000000000-0', '0000-0000-0000', '00-0000000', 'mitamanangan@gmail.com', 'F3y5wJDQNxe8rpN::tajIph+8wVgf/QsFpLVr8VbopVqtW4IEcqdQbvNOJjcHBHk=', 1, '*1li54Ei8HuYWuB3hsdMpASSEzxaYvWLLNNuwXvs2lqfYX0oetF26qce39M6lo7FkSYowMJesVS88dAO83ZdHul2ZvBO/FEqtlLxI1YcvhjNmuj4zr*DGRCyqZ8gtyC2W1d1Vgg3ns8dgBNkX6kfhAzgBgOw7hL3ABEvL0H09t4bLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '5SrtHyEeowFt1AG3F0o8vwu/UpukSm1beuAxOarNJoYVA1DVsmQ5cDs6vn2iTjKiWrvF4rfT1WyGu3Da5hKt0/sTGQYLbRFURDoSg8Phnnh1JhpAypqBl3updZN3cfXvVH9PwY8OHvy1aqRdepq8Vk4c4fPwiaAU3iVogdnQCHIbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'AABi*tzGPg/*0efUAr0DFEyVLV6xSunud1SP7veIaPIHnD0G7vya9UosfUDRYap6w8AG/aabt0uZA0E3p51popIR/2AVzaJVtW1yD8HpBtE6PgdqaXtxvlotqXNMiLws7SzBJgwFUTJvXonIBr3WPCTjzLWYi**UDUW/iuX4syobLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'UrsHQzgLNzLiiCF6NMcqT17tGCeK*clteN*uEQQktYa2*YGxnCJbZovcLtC5aOG/Ko9O4GcpyS2KBdOVb*6lxKQXejkNz1NqMR6FOLTSjvWeE2MO71aQ4rb*HdPgr060KzGyO5009IFnrxhKQHhPlwnAIYeNA4z9w3bRGv59mtgbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2015-02-04 06:27:42', '2015-02-04 05:28:24'),
(62, '1135', 20, 'Mariezzabelle', 'Cruz', 'Rosete', 15000, 'Single', 'no_avatar.jpg', '', '279488820', '2015-02-04', '74A Pao corner Sta. Catalina Sts. ', 'Barangay St. Peter', 'Quezon City, Philippines', '896-78-9067', '87-86543567890-7', '8754-3456-7890', '87-6543245', 'mariz.rosete@gmail.com', 'SfSUgdPkCVfHGo6::gStRI5mmElQ0Y1/lz92Skj/Bc9oTLfzhKf3j6Q==', 1, 'MI6Lrpu0UHhTwQCkqpdlsHkpWFS8o9s2EaROmvZZwxjMu6b1sq5zpETkmPY1BvZW4WoL981qI8TQekXL*OPezCkB1xd6PPx*k7P8AEoEnjM6JIqqV5MT79nD/9I38DGIZf1xRAJG7hApVL5/lHh*wHpp47TjBoWbUe9EqZDtzawNEsS/TKXQNapoYYYBB1UgGy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'FvioyPzoG2pT4ToWpcnoZExAUiCJeckQSsPoScx6UWczz182y1NE2CiZtBDxV6fO0pFRyG804bQqIM0*xW7crfRVDH4A568dQgeb3uS4Y/FEVgNBeHmiTSsDBEWo*NqWETsMhm8uczaY73JCdT0NnVCguyeGwV7srUZ/ELNAl0XU5c98eU68RSpQpLSJGB/uUtoS2NKGilXKTBdrHWdnrxstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'Or5m2*ghGRyXG8VjtzcXSjjQ5UxLb8axnZQypvYcKQOANpz8aE9vP0slymfZPTF0owbSVyyVj7C7gdcE9*jNVW4rj9LeHmkXPrKTZHpJ7ZEwJDuD1DwOA4ioW0*4p4/f78D95dcA2K1YscBflLzVxKBpCeo3hryYpgWxcajwMCo7SRQz8s67pAIu5B9GGDtBUpLPuKrMCWPdLX/c6jFg1iR7g*6FObF24KDGBJdTNikM0hOFMlKtabKKxG/UMmGcGy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'Or5m2*ghGRyXG8VjtzcXSgFZnmQ*hKjOj4dIiU*roAJcPxonY3cpk*h68DvcuP/pSowEPIlAra/jJUDJegdeqeChycJ9YA42CDo/dfZ5nqh3GF1r1P9kP5*zG7GV3eZQ78D95dcA2K1YscBflLzVxJaVa4TYGYFa3eoThqqNrDK7kgQwTiwz8ZJoZh0kYt9MBDyOsBy03uLJIBFimbYrJ06v2iSBz1tyi7PzJPINd/sM0hOFMlKtabKKxG/UMmGcGy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2015-02-04 06:34:00', '2015-02-04 05:35:20'),
(63, '1136', 20, 'Mary Anghelli', 'Balatbat', 'Lingo', 15000, 'Single', 'no_avatar.jpg', '', '174183853', '2015-02-04', '8 josefina St. 3rd Avenue', 'Grace Park', 'Caloocan City', '456-78-9354', '45-23456788967-8', '3434-5324-5654', '35-4789765', 'anghellilingo@gmail.com', '1kDxxc9A7RGTszK::hmHOkSk6C5iR1/TWw7ZrdCYpdFgdGOLjo4g5wlKHL6c=', 1, 'FPA2Jt*okW74pLmu0NlVYK7gORml0ju2lzxjkVvOolp0fLHEuTDHj505vMCpHXYQlg1ngpzwoyxnT0I2namGigF1l0wE9x*8nDs1dODDxRsuuswwZwlWC4yvEnc//MenZ8NhwEldEzNV9qTjvMNL5VA5m9UzHUbHO0Dv1QdAEanAmTqpOZSttAInKFWkEBTSN9JgE1d7MC/2BCU0GxvpvRstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'g8giHLSa7zZ9wQy/s3Zbcz4Nto4K9zWJUIjp2GSutVbU19TDvyiXVrZKdygiz6DUNUo8y0eUpYHX*NGvox7zIrJsIQCpP/8KWolVnmjGardO6Lvw2MwUbRrDFhfU93IyO7EajTKjQYr8JGe/Cc6PV*paIVfaHNUS54Vs*hQ7JDBcJW/oSUas3KnNHMECdBGtm/I*ZdJVRv96LYp0SFvR9RstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'fzzkXiio75yJZVQKsyhIMmI*8MIOeqt0pxFZzMe6udWSaMoijT1jSHUddE2oVWQu493ljqT/93jP31QEvwnldxQj/XJNbQC06JEP69rgXaXvN7qmTZGhwU3jlVJr8HmpKt27mgrN8KrshywW07j9NRo41fJIsHTaQj9KC16*YG7PjGun0Mq**zHR3/J6lFd7HDqCKyb3p3p3VDLEQsrJIBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'yQqiI2byCs6Bsg2hTqSGe65nCckwvvelXv8Qg8C7zy3WsOQccV944RxgSPbPItrwaqtwX9iZirBXK9Dy416VRZiOEvJ/SRL4qgvxZWfjdlqMKUcKE98lT6ucXuP*KLUO6H5mYbppTN8lJN32uuGwFkRBjMpcUG/IRKv3b9ccaX0B7W1d1e5NROjk*yRDhweVv4N6KE7Bmw8ZyG*9Cgd4BBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2015-02-04 06:40:02', '2015-02-04 05:40:45'),
(64, '1137', 25, 'Ma. Theresa', 'Basabas', 'Celada', 20000, 'Married', 'no_avatar.jpg', '', '2421382', '2015-02-04', '#241 L. Garcia St.', '899', 'Manila', '123-45-6787', '34-56432456789-0', '4567-8909-8765', '34-5678998', 'tess_celada@yahoo.com', 'MAXwydUpCYcjwK6::kcfrE6mNZya+BeROx6wB58wbtRqvJVIAwbfXGk/9gw==', 1, 'plg8vHXwTzjyTfTJFg7wTKV/uOAr1Qu6PbbOE7TapKR3dJri0oJB5ayAAWFmEClO7kiFg/Cz/v/QfPnUfyjoDXgGGemugOAvCGTj4bgzoLtOltSf65/7wqTvQ7rOoZmfZyI/I0Ga2nhHyfCtKDoW7gQcBw1X81FtE57pjx/tvgdiiFj4*ItbXGf*BErPg5JKyD66*NHWsGhPa7pIg*9TLRstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'XkUVGdfpkEwWcjnaeDW1I1dTxKyTaXI2k*h9/A81SSEan3JkbRF/Pkdkc9hVEFVxxTuWBqx6/zkdyWP/rIn6mpuqow9W1cN6aAbsJGO8MEAzDqtSVE3S*H5nH*meXQ0nx3peCfkviyHJlx4nyyC8g8aZcPIeA1j33GYLq5zIZNd8Oec7YTsVpE7bTqnqOLk8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '55iHNESZUWv3cTzfnX2a6eNDTyioL9UUjOi*KxjsFLIlSM8amNYrehxIkeDYnMGoQBWihgI4YfEeUaac2yER39BjXsgephW6GSg9INBzTDAFveFKkqYBzFoeQ/AkgjF1WcajsGdIKnNkZtvYKofDe9OVv2lpw9mBfTL5sVyCS/se/PqqFlmJauedFacGPCER3iZf/AwXGg1reYggyeNo7RstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'XkUVGdfpkEwWcjnaeDW1I4ZQRu5wtWtRYEATvGv934bEGBCGFjkxZFRUXH0MictMNI*qq2e0VWB7TwZxbW7dcmRGY9w15j0Se/Ttd4m01eivloCiwQ8MKvV3g3Pd*3zUI9vfLgDUQsJeBw4H7aBtI/m//Q07l8GAclQpx6cBGDBJxR6YYjq6JdWTfKp9QvR4Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2015-02-04 06:48:18', '2015-02-04 05:49:03'),
(65, '1138', 25, 'Grace', 'Teves', 'Sarmiento', 0, 'Single', 'no_avatar.jpg', '', '255151371', '0000-00-00', 'Blk. 3 Lot 18 Bagong Purok', 'Brgy. San Jose', 'Antipolo, City', '989-76-5456', '98-76534256789-0', '9876-5432-5789', '09-8765432', 'graceteves@gmail.com', 'ya6f1JfaqYkjvOr::MBm+muF5eYo0jOCliGObBbIDQ7ModXWV2EyJHWf6KMRv', 1, '55iHNESZUWv3cTzfnX2a6U6ZkQzUQuSk7mMwnLfg0nz5qHKGI474u5NmVdo/MCkk1u*Fevc7atCkj6fovH5oNCnq8usQ8LUflN295qoCn2Pt5YWheFT94o4uQyzP3YOZ0ZtbxArBXwdiJ71FT7i2gRVNGZxWIkx7QuXjSkEx/8Yf*fAeN1hWXjiX03uK5FWyNGcbIFmIaXdu9YRT4m3YnRstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'WQJ4h0pEp*ob2u8lGuH1mcx87bmDgNryFt2cPO1KH0Q2lZl2OqdasEPwjnPog1wRO6xhbGAfdLmuQICJm0MH8ecXCWP5693dgffPLf54uFjkWv/5P9CU2UHVSgpNnEXE/3FbylXGOnYmOY0kJ4ZsxvtxcEkjEYoxt5nbIS0hPBPtWwSHcmCex3GjwB2kGQbL1*ediQjuLH0LTv58bFU*BvCXzvhgDkhxY7ynNenlgZUbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'PxR7cf0vJG4Pdgx6T8lYcHEo4EmpFflTaH/n258c2zTxHauFfHb/Q28EuiMPX6bbTIHzt0yj9tByHUQQ*7kfzicd8dOl4sM2sGnE4QsWe5Z4b23Eu8qMiDYxQ28yPr7NPG9subLABt*XdJRjqnDXCbUcIAG4VclvxE3672K*vaBKDafZrmuOKZowbs2Vbx9YKr1udqEpFiOaPFMhuKSP0Lx3R56qMqNgxizStimn92sbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'NY7svs2E9itwggbUD73TZux4TMeL6Awi/gXVMmIhG0pNK1T8UavmUd35tbSFpb3jJKeWpKjknnvNnVzRA557bPVY6TOiSoK3zd4IDGhf4DuDukTfDTsZ7CtiS7Qqj0ya37kFr5OWpT8fUP9t0wG*tLq4iQccQXGE9FFSzXkBESVNogKGVdiDJbUiyhaJcH/LzHeyP8yn1avHr63xR4GFiFhvbXfWhOXNOPrD8hN6pwoKJfpQRAfgt8NTk8GeyGsTGy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2015-02-04 07:01:15', '2015-02-04 06:04:37'),
(66, '1139', 17, 'virgie', 'iguet', 'agapito', 25000, 'Single', 'no_avatar.jpg', '', '16407657', '2015-02-04', '428 ordonez compr.medicion 1, imus, cavite', '', '', '234-56-7890', '45-67809876543-2', '3456-7809-8765', '90-8776578', 'vagapito@gmail.com', 'Ek3vHdvFsjAOXLh::cYmYZUrGMXVxVxtECAbjpkWkBbl1Lg55ulM=', 1, 'E41PtKwzuAg9o4w3hjQdA3a2PVpJAfzT7xuqBWq9fOlfgsER6vp*6tpGjmzWtDFAgj5mJP4apP3aORQy0fDY1ZlnSLYqGxB8K/Sli6U5jcKnCiHAA2ZaToqr6GRj76vkGy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'UO5xQzK5sDuKxIlG7ZRn2Mru3l32TVfHoJj08yOfkb6KjI0lKCnWDS6UMXrm8molWMTaKULLdaqkG9xqcW95beZf6MAgKvFw44xzvXEzl78bLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'DS0c7npmRnaBO0FObRRF8r6A2T8ghIZWnIOje7I5gWaydsrCUxOrUrKtq9Eo7lX1tEEh57UMDb*rEYOYRVT76dk83CCP1mAk09WMaOkSfnEbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', 'q5SF2ftkNtzVQsI54RraCv2WrAEyFK4Js7g/1ewfcNho2Ruqia2spYXfSH7t8NrllA6M34QNH8XL5Thp2nFP2JwJDavkl59Aqi0/P6zjzw/y0rTsRyQVlL2r9*OEPhF9Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fBstyeuvDV9gKmT/R/Bs*XwbLcnrrw1fYCpk/0fwbPl8Gy3J668NX2AqZP9H8Gz5fA', '2015-02-04 07:46:34', '2015-02-04 06:47:16'),
(68, '1140', 20, 'Babet', 'babet', 'Babet', 20000, 'Single', 'avatar_54e018b87d8af.jpg', '', '926175638', '2015-02-15', '3781', 'khdjkhjk', 'uHDJKA', '098-94-2784', '53-56789089898-0', '7575-4325-4467', '46-4675475', 'bebet@gmail.com', 'QdwvI0Oa3hjCFq2::Oi5XFtI+AhugGvg1RDfQ78rYRsCpJ3oXpo6o6g==', 1, '', '', '', '', '2015-02-15 04:57:28', '2015-02-15 03:57:28'),
(69, '1141', 23, 'auygu', 'ghjuagdyuj', 'hiuha', 10000, 'Single', 'avatar_54e019414558e.jpg', '', '877856456', '2015-02-15', 'gtyr', 'yu', 'ri', '676-67-7777', '57-89890986655-4', '7898-7654-6789', '78-6554435', 'gdfjh@gmail.com', 'JbU6AeM20FcC7eU::hVJNPBDxSiHnPtl+EuRuCSe64LZcujIpklWgYg==', 1, '', '', '', '', '2015-02-15 04:58:24', '2015-02-15 03:58:24');

-- --------------------------------------------------------

--
-- Table structure for table `_temprequest`
--

CREATE TABLE IF NOT EXISTS `_temprequest` (
`emprequest` int(11) NOT NULL,
  `leavesettings_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `approve_by` int(11) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `reason` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `request_status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_temprequest`
--

INSERT INTO `_temprequest` (`emprequest`, `leavesettings_id`, `employee_id`, `approve_by`, `date_from`, `date_to`, `reason`, `created_at`, `updated_at`, `request_status`) VALUES
(1, 1, 46, 0, '2015-02-12', '2015-02-12', '<span style="font-weight: bold;">Goodmorning</span>', '2015-01-29 00:00:00', '2015-01-29 01:45:46', 2),
(2, 1, 47, 0, '2015-02-15', '2015-02-15', 'good afternoon', '2015-02-01 00:00:00', '2015-02-01 06:03:16', 1),
(3, 1, 51, 0, '2015-02-15', '2015-02-15', '\r\n<p>dear mam/sir,</p>\r\n<p>&nbsp;</p>\r\n<p>Here is the example of leave request message,</p>', '2015-02-01 00:00:00', '2015-02-04 06:43:04', 1),
(4, 1, 64, 0, '2015-02-18', '2015-02-18', 'good afternoon', '2015-02-04 00:00:00', '2015-02-04 05:52:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `_tleavesettings`
--

CREATE TABLE IF NOT EXISTS `_tleavesettings` (
`leavesettings_id` int(11) NOT NULL,
  `leave` varchar(254) NOT NULL,
  `days_prior` int(11) NOT NULL,
  `number_of_days` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_tleavesettings`
--

INSERT INTO `_tleavesettings` (`leavesettings_id`, `leave`, `days_prior`, `number_of_days`, `update_at`) VALUES
(1, 'Vacation Leave', 14, 10, '2014-08-03 08:18:24'),
(2, 'Sick Leave', 0, 10, '2014-08-03 08:19:10'),
(3, 'Emergency Leave', 0, 5, '2014-08-03 08:19:10'),
(4, 'Overtime', 0, 0, '2015-02-21 14:23:26');

-- --------------------------------------------------------

--
-- Table structure for table `_tpayslips`
--

CREATE TABLE IF NOT EXISTS `_tpayslips` (
`payslip_id` int(11) NOT NULL,
  `cutoff` varchar(254) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `created_at` datetime NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_tposition`
--

CREATE TABLE IF NOT EXISTS `_tposition` (
`position_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `position` varchar(254) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_update` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_tposition`
--

INSERT INTO `_tposition` (`position_id`, `department_id`, `position`, `date_created`, `last_update`, `status`) VALUES
(2, 4, 'Accounting Clerk', '2014-05-18 13:24:00', '0000-00-00 00:00:00', 1),
(4, 5, 'Sales', '2014-05-18 14:58:24', '0000-00-00 00:00:00', 1),
(8, 6, 'Technician', '2014-05-18 14:58:24', '0000-00-00 00:00:00', 1),
(10, 10, 'Operational Manager', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(11, 12, 'inventory officer', '2014-05-18 13:24:00', '0000-00-00 00:00:00', 1),
(15, 4, 'asdfasdftest', '2014-05-18 14:57:43', '0000-00-00 00:00:00', 1),
(13, 4, 'htello', '2014-05-18 14:58:19', '0000-00-00 00:00:00', 1),
(14, 4, 'test', '2014-05-18 14:38:23', '0000-00-00 00:00:00', 1),
(16, 20, 'Manager', '2014-08-17 05:07:44', '0000-00-00 00:00:00', 1),
(17, 30, 'Manager', '2014-09-26 18:57:40', '0000-00-00 00:00:00', 1),
(19, 25, 'Staff', '2015-02-01 05:57:26', '0000-00-00 00:00:00', 1),
(20, 27, 'Editor', '2014-10-01 15:15:43', '0000-00-00 00:00:00', 1),
(25, 31, 'Layout Artist', '2015-02-01 05:58:15', '0000-00-00 00:00:00', 1),
(22, 29, 'Graphic Designer', '2014-10-01 15:17:09', '0000-00-00 00:00:00', 1),
(23, 26, 'Agent', '2014-10-01 15:17:38', '0000-00-00 00:00:00', 1),
(24, 28, 'Author', '2014-10-01 15:18:02', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `_xacl`
--

CREATE TABLE IF NOT EXISTS `_xacl` (
`xacl_id` int(11) NOT NULL,
  `xparentmodule_id` int(11) NOT NULL,
  `xroles_id` int(11) NOT NULL,
  `_xcreate` int(11) NOT NULL,
  `_xread` int(11) NOT NULL,
  `_xupdate` int(11) NOT NULL,
  `_xdelete` int(11) NOT NULL,
  `_xexport` int(11) NOT NULL DEFAULT '0',
  `_xprint` int(11) NOT NULL DEFAULT '0',
  `_xupload` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_xacl`
--

INSERT INTO `_xacl` (`xacl_id`, `xparentmodule_id`, `xroles_id`, `_xcreate`, `_xread`, `_xupdate`, `_xdelete`, `_xexport`, `_xprint`, `_xupload`) VALUES
(41, 3, 2, 1, 1, 1, 1, 1, 1, 0),
(42, 4, 2, 1, 1, 1, 1, 1, 1, 0),
(44, 5, 2, 1, 1, 1, 1, 1, 1, 0),
(45, 2, 2, 0, 1, 0, 0, 0, 0, 0),
(48, 2, 1, 0, 1, 0, 0, 0, 0, 0),
(49, 1, 1, 1, 1, 1, 1, 1, 1, 0),
(76, 3, 1, 1, 1, 1, 1, 1, 1, 0),
(77, 4, 1, 1, 1, 1, 1, 1, 1, 0),
(78, 5, 1, 1, 1, 1, 1, 1, 0, 0),
(79, 1, 2, 1, 0, 0, 0, 0, 0, 0),
(83, 2, 10, 0, 1, 0, 0, 0, 0, 0),
(84, 9, 10, 0, 1, 0, 0, 0, 0, 0),
(85, 14, 10, 1, 1, 1, 1, 1, 0, 0),
(86, 15, 10, 1, 1, 1, 1, 0, 0, 0),
(87, 7, 10, 1, 1, 1, 1, 1, 1, 0),
(88, 5, 10, 1, 1, 1, 1, 1, 1, 0),
(89, 9, 2, 0, 1, 0, 0, 0, 0, 0),
(90, 14, 2, 1, 1, 1, 1, 1, 0, 0),
(91, 15, 2, 1, 1, 1, 1, 0, 0, 0),
(92, 7, 2, 1, 1, 1, 1, 1, 1, 0),
(93, 6, 2, 1, 1, 1, 1, 1, 1, 0),
(94, 13, 2, 0, 1, 0, 0, 1, 1, 0),
(95, 8, 2, 1, 1, 1, 1, 1, 1, 0),
(96, 10, 2, 0, 1, 1, 0, 0, 0, 0),
(97, 11, 2, 1, 1, 1, 1, 1, 1, 0),
(98, 2, 11, 0, 1, 0, 0, 0, 0, 0),
(99, 6, 11, 1, 1, 1, 1, 1, 0, 1),
(100, 13, 11, 0, 1, 0, 0, 1, 1, 0),
(101, 8, 11, 0, 1, 1, 1, 0, 0, 0),
(103, 16, 2, 0, 1, 1, 0, 0, 1, 0),
(104, 16, 11, 0, 1, 1, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `_xmodule`
--

CREATE TABLE IF NOT EXISTS `_xmodule` (
`xmodule_id` int(11) NOT NULL,
  `xparentmodule_id` int(11) NOT NULL,
  `module` varchar(254) NOT NULL,
  `_xurl` varchar(254) NOT NULL,
  `_create` int(11) NOT NULL DEFAULT '0',
  `_read` int(11) NOT NULL DEFAULT '1',
  `_update` int(11) NOT NULL DEFAULT '0',
  `_delete` int(11) NOT NULL DEFAULT '0',
  `_export` int(11) NOT NULL DEFAULT '0',
  `_print` int(11) NOT NULL DEFAULT '0',
  `date_created` int(11) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_xmodule`
--

INSERT INTO `_xmodule` (`xmodule_id`, `xparentmodule_id`, `module`, `_xurl`, `_create`, `_read`, `_update`, `_delete`, `_export`, `_print`, `date_created`, `last_update`, `status`) VALUES
(1, 5, 'testsub', 'testsub', 1, 1, 1, 1, 1, 1, 0, '2014-04-17 05:22:58', 1);

-- --------------------------------------------------------

--
-- Table structure for table `_xparentlabel`
--

CREATE TABLE IF NOT EXISTS `_xparentlabel` (
`xparentlabel_id` int(11) NOT NULL,
  `label` varchar(254) NOT NULL,
  `creation_date` datetime NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_xparentlabel`
--

INSERT INTO `_xparentlabel` (`xparentlabel_id`, `label`, `creation_date`, `last_update`, `status`) VALUES
(1, 'Home', '2014-04-13 14:00:00', '2014-04-17 01:52:22', 1),
(2, 'System Settings', '2014-04-13 12:00:00', '2014-04-17 01:51:29', 1),
(3, 'Human Resource', '2014-04-17 00:00:00', '2014-04-17 12:56:18', 1),
(4, 'Accounting', '2014-04-17 00:00:00', '2014-04-17 12:56:34', 1),
(5, 'Payroll Settings', '2014-05-28 00:00:00', '2014-05-13 13:23:12', 1),
(6, 'Reports', '2014-05-13 00:00:00', '2014-05-13 14:41:56', 1);

-- --------------------------------------------------------

--
-- Table structure for table `_xparentmodule`
--

CREATE TABLE IF NOT EXISTS `_xparentmodule` (
`xparentmodule_id` int(11) NOT NULL,
  `xparentlabel_id` int(11) NOT NULL,
  `parentmodule` varchar(254) NOT NULL,
  `_xurl` varchar(254) NOT NULL,
  `_xcreate` int(11) NOT NULL DEFAULT '0',
  `_xread` int(11) NOT NULL DEFAULT '0',
  `_xupdate` int(11) NOT NULL DEFAULT '0',
  `_xdelete` int(11) NOT NULL DEFAULT '0',
  `_xprint` int(11) NOT NULL DEFAULT '0',
  `_xexport` int(11) NOT NULL DEFAULT '0',
  `_xupload` int(11) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_xparentmodule`
--

INSERT INTO `_xparentmodule` (`xparentmodule_id`, `xparentlabel_id`, `parentmodule`, `_xurl`, `_xcreate`, `_xread`, `_xupdate`, `_xdelete`, `_xprint`, `_xexport`, `_xupload`, `date_created`, `last_update`, `status`) VALUES
(1, 2, 'modules', 'modules', 1, 1, 1, 1, 1, 1, 0, '2014-04-13 09:22:00', '2014-04-13 11:48:55', 1),
(2, 1, 'Dashboard', 'home', 0, 1, 0, 0, 0, 0, 0, '2014-04-13 09:00:00', '2014-04-27 02:31:22', 1),
(3, 2, 'Role', 'role', 1, 1, 1, 1, 1, 1, 0, '2014-04-23 09:25:00', '2014-04-13 11:47:15', 1),
(4, 2, 'Users', 'users', 1, 1, 1, 1, 1, 1, 0, '2014-04-13 11:26:00', '2014-04-17 01:59:45', 1),
(5, 3, 'Employees', 'employees', 1, 1, 1, 1, 1, 1, 0, '2014-04-17 00:00:00', '2014-05-13 13:08:49', 1),
(6, 4, 'Daily attendance', 'daily-attendance', 1, 1, 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '2015-01-26 03:32:25', 1),
(7, 3, 'Leave Request', 'leave-request', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '2015-01-26 05:16:07', 1),
(8, 4, 'Overtime', 'overtime', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '2015-02-21 23:38:43', 0),
(9, 2, 'Testing', 'testing', 0, 1, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '2014-07-13 07:18:24', 0),
(10, 5, 'General Settings', 'general-settings', 0, 1, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', '2014-05-13 13:38:12', 1),
(11, 5, 'Flexitime Settings', 'flexitime-settings', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '2015-01-26 03:40:16', 0),
(12, 5, 'Holiday Settings', 'holiday-settings', 1, 1, 1, 1, 0, 0, 0, '0000-00-00 00:00:00', '2014-06-07 23:59:43', 0),
(13, 4, 'Payslip', 'payslip', 0, 1, 0, 0, 1, 1, 0, '0000-00-00 00:00:00', '2014-05-13 14:43:26', 1),
(14, 3, 'Department management', 'department', 1, 1, 1, 1, 0, 1, 0, '0000-00-00 00:00:00', '2014-05-14 13:21:34', 1),
(15, 3, 'Position management', 'positions', 1, 1, 1, 1, 0, 0, 0, '0000-00-00 00:00:00', '2014-05-18 13:18:35', 1),
(16, 4, 'Payroll', 'payroll', 0, 1, 1, 0, 1, 0, 0, '0000-00-00 00:00:00', '2015-01-30 19:36:23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `_xroles`
--

CREATE TABLE IF NOT EXISTS `_xroles` (
`xroles_id` int(11) NOT NULL,
  `role` varchar(254) NOT NULL,
  `date_created` datetime NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_xroles`
--

INSERT INTO `_xroles` (`xroles_id`, `role`, `date_created`, `last_update`, `status`) VALUES
(1, 'System Developer', '2014-04-13 12:00:00', '2014-05-11 13:55:01', 0),
(2, 'System Administrators', '2014-04-13 13:04:00', '2014-07-13 04:42:55', 1),
(10, 'Human Resource', '2014-07-06 03:32:11', '2014-07-06 03:32:11', 1),
(11, 'Accounting', '2015-01-26 04:37:52', '2015-01-26 03:37:52', 1);

-- --------------------------------------------------------

--
-- Table structure for table `_xusers`
--

CREATE TABLE IF NOT EXISTS `_xusers` (
`xusers_id` int(11) NOT NULL,
  `email` varchar(254) NOT NULL,
  `password` varchar(254) NOT NULL,
  `first_name` varchar(254) NOT NULL,
  `middle_name` varchar(254) NOT NULL,
  `last_name` varchar(254) NOT NULL,
  `avatar` varchar(254) NOT NULL,
  `date_created` datetime NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL,
  `xroles_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_xusers`
--

INSERT INTO `_xusers` (`xusers_id`, `email`, `password`, `first_name`, `middle_name`, `last_name`, `avatar`, `date_created`, `last_update`, `status`, `xroles_id`) VALUES
(1, 'king.marikudo@gmail.com', 'Fr2qsjllEbkfRz4::isjBZzw2ujkcUwjGvi1F5LXYfdDc7JMMCg==', 'Blue', 'R', 'Pink', '', '2014-04-13 12:00:00', '2015-02-21 14:04:54', 1, 1),
(2, 'administrator@system.com', 'Fr2qsjllEbkfRz4::isjBZzw2ujkcUwjGvi1F5LXYfdDc7JMMCg==', 'Administrator', 'Administratorx', 'Administrator', '', '2014-04-13 12:15:00', '2014-07-13 07:22:12', 1, 2),
(4, 'hr@system.com', 'mW9ZwyySlqLg5nI::rMLLJn0n4oEzzMHoM+UQEGT80VeMSXsd4A==', 'Cathy', 'Cat', 'Cat', '', '2014-07-06 03:33:43', '2015-02-01 05:54:33', 1, 10),
(6, 'accounting@system.com', 'MNCt2m4hLm0ufGM::M7YXFonwHozAQjA857ir5rK0YRRMH3McWQ==', 'Accounting', 'Accounting', 'Accounting', '', '2015-01-26 04:44:09', '2015-02-01 14:51:57', 1, 11),
(7, 'hrd@system.com', 'KkvbYp5N89PIXsr::N3HDw/CXh8E87oi1TdY83naG8WhChoW+6LE=', 'hr', 'hr', 'hr', '', '2015-02-15 03:47:33', '2015-02-15 02:47:33', 1, 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `_holidays`
--
ALTER TABLE `_holidays`
 ADD PRIMARY KEY (`holiday_id`), ADD KEY `holiday_type` (`holiday_type`);

--
-- Indexes for table `_holiday_type`
--
ALTER TABLE `_holiday_type`
 ADD PRIMARY KEY (`holiday_type_id`);

--
-- Indexes for table `_tdailytimerecord`
--
ALTER TABLE `_tdailytimerecord`
 ADD PRIMARY KEY (`dailytimerecord_id`), ADD KEY `eid` (`eid`), ADD KEY `employee_id` (`employee_id`);

--
-- Indexes for table `_tdepartment`
--
ALTER TABLE `_tdepartment`
 ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `_temployee`
--
ALTER TABLE `_temployee`
 ADD PRIMARY KEY (`employee_id`), ADD UNIQUE KEY `eid` (`eid`,`email`), ADD KEY `position_id` (`position_id`);

--
-- Indexes for table `_temprequest`
--
ALTER TABLE `_temprequest`
 ADD PRIMARY KEY (`emprequest`), ADD KEY `leavesettings_id` (`leavesettings_id`,`employee_id`,`approve_by`);

--
-- Indexes for table `_tleavesettings`
--
ALTER TABLE `_tleavesettings`
 ADD PRIMARY KEY (`leavesettings_id`);

--
-- Indexes for table `_tpayslips`
--
ALTER TABLE `_tpayslips`
 ADD PRIMARY KEY (`payslip_id`);

--
-- Indexes for table `_tposition`
--
ALTER TABLE `_tposition`
 ADD PRIMARY KEY (`position_id`), ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `_xacl`
--
ALTER TABLE `_xacl`
 ADD PRIMARY KEY (`xacl_id`), ADD KEY `xroles_id` (`xroles_id`), ADD KEY `xparentmodule_id` (`xparentmodule_id`);

--
-- Indexes for table `_xmodule`
--
ALTER TABLE `_xmodule`
 ADD PRIMARY KEY (`xmodule_id`), ADD UNIQUE KEY `module` (`module`), ADD UNIQUE KEY `_xurl` (`_xurl`), ADD KEY `parentlabel_id` (`xparentmodule_id`);

--
-- Indexes for table `_xparentlabel`
--
ALTER TABLE `_xparentlabel`
 ADD PRIMARY KEY (`xparentlabel_id`);

--
-- Indexes for table `_xparentmodule`
--
ALTER TABLE `_xparentmodule`
 ADD PRIMARY KEY (`xparentmodule_id`), ADD UNIQUE KEY `parentmodule` (`parentmodule`), ADD UNIQUE KEY `_xurl` (`_xurl`), ADD KEY `parentlabel_id` (`xparentlabel_id`);

--
-- Indexes for table `_xroles`
--
ALTER TABLE `_xroles`
 ADD PRIMARY KEY (`xroles_id`), ADD UNIQUE KEY `role` (`role`);

--
-- Indexes for table `_xusers`
--
ALTER TABLE `_xusers`
 ADD PRIMARY KEY (`xusers_id`), ADD UNIQUE KEY `email` (`email`), ADD KEY `xroles_id` (`xroles_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `_holidays`
--
ALTER TABLE `_holidays`
MODIFY `holiday_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `_holiday_type`
--
ALTER TABLE `_holiday_type`
MODIFY `holiday_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `_tdailytimerecord`
--
ALTER TABLE `_tdailytimerecord`
MODIFY `dailytimerecord_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_tdepartment`
--
ALTER TABLE `_tdepartment`
MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `_temployee`
--
ALTER TABLE `_temployee`
MODIFY `employee_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `_temprequest`
--
ALTER TABLE `_temprequest`
MODIFY `emprequest` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `_tleavesettings`
--
ALTER TABLE `_tleavesettings`
MODIFY `leavesettings_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `_tpayslips`
--
ALTER TABLE `_tpayslips`
MODIFY `payslip_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_tposition`
--
ALTER TABLE `_tposition`
MODIFY `position_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `_xacl`
--
ALTER TABLE `_xacl`
MODIFY `xacl_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT for table `_xmodule`
--
ALTER TABLE `_xmodule`
MODIFY `xmodule_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `_xparentlabel`
--
ALTER TABLE `_xparentlabel`
MODIFY `xparentlabel_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `_xparentmodule`
--
ALTER TABLE `_xparentmodule`
MODIFY `xparentmodule_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `_xroles`
--
ALTER TABLE `_xroles`
MODIFY `xroles_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `_xusers`
--
ALTER TABLE `_xusers`
MODIFY `xusers_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `_xacl`
--
ALTER TABLE `_xacl`
ADD CONSTRAINT `_xacl_ibfk_2` FOREIGN KEY (`xroles_id`) REFERENCES `_xroles` (`xroles_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `_xacl_ibfk_4` FOREIGN KEY (`xparentmodule_id`) REFERENCES `_xparentmodule` (`xparentmodule_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `_xmodule`
--
ALTER TABLE `_xmodule`
ADD CONSTRAINT `_xmodule_ibfk_1` FOREIGN KEY (`xparentmodule_id`) REFERENCES `_xparentmodule` (`xparentmodule_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `_xparentmodule`
--
ALTER TABLE `_xparentmodule`
ADD CONSTRAINT `_xparentmodule_ibfk_1` FOREIGN KEY (`xparentlabel_id`) REFERENCES `_xparentlabel` (`xparentlabel_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `_xusers`
--
ALTER TABLE `_xusers`
ADD CONSTRAINT `_xusers_ibfk_1` FOREIGN KEY (`xroles_id`) REFERENCES `_xroles` (`xroles_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
